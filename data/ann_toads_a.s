    .export ann_toad_1
ann_toad_1:
    .incbin "./chr_misc.ann/chr_obj_00.bin"
    .incbin "./chr_misc.ann/chr_obj_01.bin"
    .incbin "./chr_misc.ann/chr_obj_02.bin"
    .incbin "./chr_misc.ann/chr_obj_03.bin"
    .incbin "./chr_misc.ann/chr_obj_04.bin"
    .incbin "./chr_misc.ann/chr_obj_05.bin"

    .export ann_toad_2
ann_toad_2:
    .incbin "./chr_misc.ann/chr_obj_06.bin"
    .incbin "./chr_misc.ann/chr_obj_07.bin"
    .incbin "./chr_misc.ann/chr_obj_08.bin"
    .incbin "./chr_misc.ann/chr_obj_09.bin"
    .incbin "./chr_misc.ann/chr_obj_0A.bin"
    .incbin "./chr_misc.ann/chr_obj_0B.bin"

    .export ann_toad_3
ann_toad_3:
    .incbin "./chr_misc.ann/chr_obj_0C.bin"
    .incbin "./chr_misc.ann/chr_obj_0D.bin"
    .incbin "./chr_misc.ann/chr_obj_0E.bin"
    .incbin "./chr_misc.ann/chr_obj_0F.bin"
    .incbin "./chr_misc.ann/chr_obj_10.bin"
    .incbin "./chr_misc.ann/chr_obj_11.bin"

    .export ann_toad_4
ann_toad_4:
    .incbin "./chr_misc.ann/chr_obj_12.bin"
    .incbin "./chr_misc.ann/chr_obj_13.bin"
    .incbin "./chr_misc.ann/chr_obj_14.bin"
    .incbin "./chr_misc.ann/chr_obj_15.bin"
    .incbin "./chr_misc.ann/chr_obj_16.bin"
    .incbin "./chr_misc.ann/chr_obj_17.bin"
