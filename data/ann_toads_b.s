.segment    "DATA2"

    .export ann_toad_5
ann_toad_5:
    .incbin "./chr_misc.ann/chr_obj_18.bin"
    .incbin "./chr_misc.ann/chr_obj_19.bin"
    .incbin "./chr_misc.ann/chr_obj_1A.bin"
    .incbin "./chr_misc.ann/chr_obj_1B.bin"
    .incbin "./chr_misc.ann/chr_obj_1C.bin"
    .incbin "./chr_misc.ann/chr_obj_1D.bin"

    .export ann_toad_6
ann_toad_6:
    .incbin "./chr_misc.ann/chr_obj_1E.bin"
    .incbin "./chr_misc.ann/chr_obj_1F.bin"
    .incbin "./chr_misc.ann/chr_obj_20.bin"
    .incbin "./chr_misc.ann/chr_obj_21.bin"
    .incbin "./chr_misc.ann/chr_obj_22.bin"
    .incbin "./chr_misc.ann/chr_obj_23.bin"

    .export ann_toad_7
ann_toad_7:
    .incbin "./chr_misc.ann/chr_obj_24.bin"
    .incbin "./chr_misc.ann/chr_obj_25.bin"
    .incbin "./chr_misc.ann/chr_obj_26.bin"
    .incbin "./chr_misc.ann/chr_obj_27.bin"
    .incbin "./chr_misc.ann/chr_obj_28.bin"
    .incbin "./chr_misc.ann/chr_obj_29.bin"
