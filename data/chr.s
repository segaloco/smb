.segment	"CHR0DATA"

	.export chr_obj, chr_bg
chr_obj:
	.include	"chr/obj/player_misc.s"
	.include	"chr/obj/digits_a.s"
	.include	"chr/obj/bowser_flame.s"

	.if	.defined(SMB)|.defined(VS_SMB)|.defined(SMB2)
		.include	"chr/obj/flag_victory.s"
	.elseif	.defined(ANN)
		.include	"chr/obj/flag_victory_nsm.s"
	.endif

	.include	"chr/obj/player_crouch.s"

	.if	.defined(SMBV1)
		.include	"chr/obj/plat_a.s"
	.elseif	.defined(SMBV2)
		.include	"chr/obj/plat_c.s"
	.endif

	.include	"chr/obj/player_climb_large.s"
	.include	"chr/obj/coin.s"
	.include	"chr/obj/fire_ball.s"
	.include	"chr/obj/fire_hit.s"
	.include	"chr/obj/koopa_a.s"

	.if	.defined(SMB)|.defined(VS_SMB)|.defined(SMB2)
		.include	"chr/obj/goomba_a.s"
	.elseif	.defined(ANN)
		.include	"chr/obj/goomba_nsm_a.s"
	.endif

	.include	"chr/obj/bubble.s"
	.include	"chr/obj/plat_b.s"

	.if	.defined(SMBV1)
		.include	"chr/obj/mushroom.s"
	.elseif	.defined(SMBV2)
		.include	"chr/obj/door.s"
	.endif

	.if	.defined(SMBV1)
		.include	"chr/obj/princess_a.s"
	.elseif	.defined(SMB2)
		.include	"chr/obj/leaf.s"
	.elseif	.defined(ANN)
		.include	"chr/obj/nsm_guy_a.s"
	.endif

	.include	"chr/obj/hammer_bro_a.s"
	.include	"chr/obj/flag_enemy.s"
	.include	"chr/obj/hammer.s"
	.include	"chr/obj/brick_piece.s"

	.if	.defined(SMBM)|.defined(VS_SMB)
		.include	"chr/obj/brick.s"
	.elseif	.defined(SMB2)
		.include	"chr/obj/brick_new.s"
	.endif

	.include	"chr/obj/block.s"
	.include	"chr/obj/hammer_bro_b.s"

	.if	.defined(SMB)|.defined(VS_SMB)|.defined(SMB2)
		.include	"chr/obj/star_top.s"
	.elseif	.defined(ANN)
		.include	"chr/obj/star_nsm_top.s"
	.endif

	.include	"chr/obj/spiny_egg_a.s"
	.include	"chr/obj/player_climb_small.s"
	.include	"chr/obj/spiny_egg_b.s"
	.include	"chr/obj/spiny.s"
	.include	"chr/obj/player_death.s"
	.include	"chr/obj/koopa_b.s"
	.include	"chr/obj/buzzy_a.s"
	.include	"chr/obj/cheep.s"
	.include	"chr/obj/lakitu.s"
	.include	"chr/obj/bowser.s"

	.if	.defined(SMB)|.defined(VS_SMB)|.defined(SMB2)
		.include	"chr/obj/toad.s"
	.elseif	.defined(ANN)
		.include	"chr/obj/nsm_guy_b.s"
	.endif

	.include	"chr/obj/podoboo_a.s"
	.include	"chr/obj/hammer_bro_c.s"
	.include	"chr/obj/firefl_top.s"
	.include	"chr/obj/podoboo_b.s"

	.if	.defined(SMBV1)
		.include	"chr/obj/princess_b.s"
	.elseif	.defined(SMBV2)
		.include	"chr/obj/mushroom_new_a.s"
	.endif

	.include	"chr/obj/firefl_stem.s"

	.if	.defined(SMBV1)
		.include	"chr/obj/princess_c.s"
	.elseif	.defined(SMBV2)
		.include	"chr/obj/mushroom_new_b.s"
	.endif

	.include	"chr/obj/blooper.s"
	.include	"chr/obj/vine.s"
	.include	"chr/obj/hammer_bro_d.s"

	.if	.defined(SMB)|.defined(VS_SMB)|.defined(SMB2)
		.include	"chr/obj/star_bottom.s"
		.include	"chr/obj/piranha_a.s"
	.elseif	.defined(ANN)
		.include	"chr/obj/star_nsm_bottom.s"
		.include	"chr/obj/piranha_nsm_a.s"
	.endif

	.include	"chr/obj/bullet.s"

	.if	.defined(SMB)|.defined(VS_SMB)|.defined(SMB2)
		.include	"chr/obj/piranha_b.s"
		.include	"chr/obj/goomba_b.s"
	.elseif	.defined(ANN)
		.include	"chr/obj/piranha_nsm_b.s"
		.include	"chr/obj/goomba_nsm_b.s"
	.endif

	.include	"chr/obj/spring.s"
	.include	"chr/obj/buzzy_b.s"
	.include	"chr/obj/digits_b.s"
	.include	"chr/obj/empty.s"

	.if	.defined(SMB)
		.include	"chr/obj/oneup.s"
	.elseif	.defined(VS_SMB)
		.include	"chr/obj/oneup_new_a.s"
	.elseif	.defined(SMBV2)
		.include	"chr/obj/oneup_new_b.s"
	.endif

	.if	.defined(SMBV1)
		.include	"chr/obj/filler.s"
	.elseif	.defined(SMBV2)
		.include	"chr/obj/mushroom_new_c.s"
	.endif

chr_bg:
	.if	.defined(SMB)
		.include	"chr/bg/font_a.s"
	.elseif	.defined(VS_SMB)|.defined(SMBV2)
		.include	"chr/bg/font_a.vs.s"
	.endif

	.if	.defined(SMB)
		.include	"chr/bg/font_b.s"
	.elseif	.defined(VS_SMB)
		.include	"chr/bg/font_b.vs.s"
	.elseif	.defined(SMBV2)
		.include	"chr/bg/font_b.smb2.s"
	.endif

	.if	.defined(SMB)
		.include	"chr/bg/font_c.s"
	.elseif	.defined(VS_SMB)|.defined(SMBV2)
		.include	"chr/bg/font_c.vs.s"
	.endif

	.if	.defined(SMB)
		.include	"chr/bg/font_d.s"
	.elseif	.defined(VS_SMB)
		.include	"chr/bg/font_d.vs.s"
	.elseif	.defined(SMBV2)
		.include	"chr/bg/font_d.smb2.s"
	.endif

	.if	.defined(SMB)
		.include	"chr/bg/font_e.s"
	.elseif	.defined(VS_SMB)|.defined(SMBV2)
		.include	"chr/bg/font_e.vs.s"
	.endif

	.include	"chr/bg/blanks.s"

	.if	.defined(SMB)
		.include	"chr/bg/font_f.s"
	.elseif	.defined(VS_SMB)|.defined(SMBV2)
		.include	"chr/bg/font_f.vs.s"
	.endif

	.include	"chr/bg/cannon_base_left.s"

	.if	.defined(SMB)
		.include	"chr/bg/font_g.s"
	.elseif	.defined(VS_SMB)|.defined(SMBV2)
		.include	"chr/bg/font_g.vs.s"
	.endif

	.if	.defined(SMBM)|.defined(VS_SMB)
		.include	"chr/bg/plat_a_mid.s"
	.elseif	.defined(SMB2)
		.include	"chr/bg/plat_a_mid_new.s"
	.endif

	.include	"chr/bg/hud.s"
	.include	"chr/bg/flag_ball_a.s"

	.if	.defined(SMBM)|.defined(VS_SMB)
		.include	"chr/bg/hill_a.s"
	.elseif	.defined(SMB2)
		.include	"chr/bg/hill_a_new.s"
	.endif

	.if	.defined(SMBV1)
		.include	"chr/bg/hill_b.s"
	.elseif	.defined(SMBV2)
		.include	"chr/bg/hill_b_new.s"
	.endif

	.if	.defined(SMBV1)
		.include	"chr/bg/cloud.s"
	.elseif	.defined(SMBV2)	
		.include	"chr/bg/cloud_new.s"
	.endif

	.include	"chr/bg/flag_ball_b.s"
	.include	"chr/bg/pulley_a.s"
	.include	"chr/bg/cannon_base_right.s"
	.include	"chr/bg/water.s"
	.include	"chr/bg/sign_letter_edge_nw.s"

	.if	.defined(SMBM)|.defined(VS_SMB)
		.include	"chr/bg/brick_a.s"
	.elseif	.defined(SMB2)
		.include	"chr/bg/brick_a_new.s"
	.endif

	.include	"chr/bg/sign_w.s"

	.if	.defined(SMBM)|.defined(VS_SMB)
		.include	"chr/bg/brick_b.s"
	.elseif	.defined(SMB2)
		.include	"chr/bg/brick_b_new.s"
	.endif

	.include	"chr/bg/sign_n_ne_e.s"

	.if	.defined(SMBV1)
		.include	"chr/bg/plat_b.s"
	.elseif	.defined(SMBV2)
		.include	"chr/bg/plat_b_new.s"
	.endif

	.include	"chr/bg/block.s"
	.include	"chr/bg/pulley_b.s"

	.if	.defined(SMBV1)
		.include	"chr/bg/floor_castle.s"
	.elseif	.defined(SMBV2)
		.include	"chr/bg/floor_castle_new.s"
	.endif

	.include	"chr/bg/sign_sw.s"
	.include	"chr/bg/pipe_v.s"

	.if	.defined(SMBM)|.defined(VS_SMB)
		.include	"chr/bg/plat_a.s"
	.elseif	.defined(SMB2)
		.include	"chr/bg/plat_a_new.s"
	.endif

	.if	.defined(SMBV1)
		.include	"chr/bg/edge_b.s"
	.elseif	.defined(SMB2)
		.include	"chr/bg/quotes.s"
	.elseif	.defined(ANN)
		.include	"chr/bg/edge_b_nsm.s"
	.endif

	.include	"chr/bg/bridge_a.s"
	.include	"chr/bg/sign_s.s"
	.include	"chr/bg/bridge_b.s"
	.include	"chr/bg/sign_se.s"

	.if	.defined(SMB)|.defined(VS_SMB)|.defined(SMB2)
		.include	"chr/bg/axe.s"
	.elseif	.defined(ANN)
		.include	"chr/bg/axe_nsm.s"
	.endif

	.include	"chr/bg/chain.s"

	.if	.defined(SMBV1)
		.include	"chr/bg/post_a.s"
	.elseif	.defined(SMB2)
		.include	"chr/bg/post_a_new.s"
	.elseif	.defined(ANN)
		.include	"chr/bg/post_a_nsm.s"
	.endif

	.include	"chr/bg/floor_underwater.s"
	.include	"chr/bg/pipe_h.s"
	.include	"chr/bg/sign_text_a.s"
	.include	"chr/bg/rope.s"

	.if	.defined(SMBM)|.defined(VS_SMB)
		.include	"chr/bg/castle_a.s"
	.elseif	.defined(SMB2)
		.include	"chr/bg/castle_a_new.s"
	.endif

	.include	"chr/bg/crown.s"

	.if	.defined(SMBV1)
		.include	"chr/bg/post_b.s"
	.elseif	.defined(SMB2)
		.include	"chr/bg/post_b_new.s"
	.elseif	.defined(ANN)
		.include	"chr/bg/post_b_nsm.s"
	.endif

	.include	"chr/bg/pole.s"
	.include	"chr/bg/coral_a.s"
	.include	"chr/bg/coin_a.s"

	.if	.defined(SMBM)|.defined(VS_SMB)
		.include	"chr/bg/castle_b.s"
	.elseif	.defined(SMB2)
		.include	"chr/bg/castle_b_new.s"
	.endif

	.include	"chr/bg/stair.s"

	.if	.defined(SMB)
		.include	"chr/bg/font_h.s"
	.elseif	.defined(VS_SMB)|.defined(SMBV2)
		.include	"chr/bg/font_h.vs.s"
	.endif

	.include	"chr/bg/cloud_small.s"

	.if	.defined(SMBV1)
		.include	"chr/bg/floor_normal.s"
		.include	"chr/bg/tree.s"
	.elseif	.defined(SMBV2)
		.include	"chr/bg/floor_normal_new.s"
		.include	"chr/bg/tree_new.s"
	.endif

	.include	"chr/bg/tree_trunk.s"

	.if	.defined(SMBV1)
		.include	"chr/bg/bridge_parts.s"
	.elseif	.defined(SMBV2)
		.include	"chr/bg/bridge_parts_new.s"
	.endif

	.include	"chr/bg/coin_b.s"
	.include	"chr/bg/cannon.s"

	.if	.defined(CONS)
		.include	"chr/bg/cursor.s"
	.elseif	.defined(VS)
		.include	"chr/bg/cursor_vs.s"
	.endif

	.if	.defined(SMBV1)
		.include	"chr/bg/copyright.s"
	.elseif	.defined(SMBV2)
		.include	"chr/bg/copyright.smb2.s"
	.endif

	.include	"chr/bg/sign_text_b.s"
	.include	"chr/bg/coral_b.s"

	.if	.defined(VS_SMB)
		.include	"chr/bg/sign_text_vs.s"
		.include	"chr/bg/window.s"
	.elseif	.defined(SMB2)
		.include	"chr/bg/sign_text_smb2.s"
		.include	"chr/bg/star_smb2.s"
		.include	"chr/bg/font_j.smb2.s"
		.include	"chr/bg/floor_un0.s"
		.include	"chr/bg/cactus.s"
		.include	"chr/bg/floor_un1.s"
	.elseif	.defined(ANN)
		.include	"chr/bg/sign_text_ann_a.s"
		.include	"chr/bg/star_smb2.s"
		.include	"chr/bg/font_j.smb2.s"
		.include	"chr/bg/sign_text_ann_b.s"
		.include	"chr/bg/quotes.s"
		.include	"chr/bg/sign_text_f.s"
	.endif

	.if	.defined(VS_SMB)|.defined(SMBV2)
		.byte	0
	.endif
