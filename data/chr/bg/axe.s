.ifndef	AXE
AXE = 1

	.export chr_bg_axe
chr_bg_axe:
		.incbin	"data/chr.smb/chr_bg_7B.bin"
		.incbin	"data/chr.smb/chr_bg_7C.bin"
		.incbin	"data/chr.smb/chr_bg_7D.bin"
		.incbin	"data/chr.smb/chr_bg_7E.bin"

.endif	; AXE
