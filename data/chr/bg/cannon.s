.ifndef	CANNON_S
CANNON_S = 1

	.export chr_bg_cannon
chr_bg_cannon:
	.incbin	"data/chr.smb/chr_bg_C6.bin"
	.incbin	"data/chr.smb/chr_bg_C7.bin"
	.incbin	"data/chr.smb/chr_bg_C8.bin"
	.incbin	"data/chr.smb/chr_bg_C9.bin"
	.incbin	"data/chr.smb/chr_bg_CA.bin"
	.incbin	"data/chr.smb/chr_bg_CB.bin"
	.incbin	"data/chr.smb/chr_bg_CC.bin"
	.incbin	"data/chr.smb/chr_bg_CD.bin"

.endif	; CANNON_S
