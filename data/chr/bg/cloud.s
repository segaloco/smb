.ifndef	CLOUD_S
CLOUD_S = 1

	.export chr_bg_cloud
chr_bg_cloud:
		.incbin	"data/chr.smb/chr_bg_35.bin"
		.incbin	"data/chr.smb/chr_bg_36.bin"
		.incbin	"data/chr.smb/chr_bg_37.bin"
		.incbin	"data/chr.smb/chr_bg_38.bin"
		.incbin	"data/chr.smb/chr_bg_39.bin"
		.incbin	"data/chr.smb/chr_bg_3A.bin"
		.incbin	"data/chr.smb/chr_bg_3B.bin"
		.incbin	"data/chr.smb/chr_bg_3C.bin"

.endif	; CLOUD
