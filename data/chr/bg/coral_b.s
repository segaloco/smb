.ifndef	CORAL_B
CORAL_B = 1

	.export chr_bg_coral_b
chr_bg_coral_b:
	.incbin	"data/chr.smb/chr_bg_E9.bin"
	.incbin	"data/chr.smb/chr_bg_EA.bin"
	.incbin	"data/chr.smb/chr_bg_EB.bin"

.endif	; CORAL
