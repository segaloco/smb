.ifndef	PIPE_H
PIPE_H = 1

	.export chr_bg_pipe_h
chr_bg_pipe_h:
	.incbin	"data/chr.smb/chr_bg_86.bin"
	.incbin	"data/chr.smb/chr_bg_87.bin"
	.incbin	"data/chr.smb/chr_bg_88.bin"
	.incbin	"data/chr.smb/chr_bg_89.bin"
	.incbin	"data/chr.smb/chr_bg_8A.bin"
	.incbin	"data/chr.smb/chr_bg_8B.bin"
	.incbin	"data/chr.smb/chr_bg_8C.bin"
	.incbin	"data/chr.smb/chr_bg_8D.bin"
	.incbin	"data/chr.smb/chr_bg_8E.bin"
	.incbin	"data/chr.smb/chr_bg_8F.bin"
	.incbin	"data/chr.smb/chr_bg_90.bin"
	.incbin	"data/chr.smb/chr_bg_91.bin"
	.incbin	"data/chr.smb/chr_bg_92.bin"
	.incbin	"data/chr.smb/chr_bg_93.bin"
	.incbin	"data/chr.smb/chr_bg_94.bin"

.endif	; PIPE_H
