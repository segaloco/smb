.ifndef	PIPE_V
PIPE_V = 1

	.export chr_bg_pipe_v
chr_bg_pipe_v:
	.incbin	"data/chr.smb/chr_bg_60.bin"
	.incbin	"data/chr.smb/chr_bg_61.bin"
	.incbin	"data/chr.smb/chr_bg_62.bin"
	.incbin	"data/chr.smb/chr_bg_63.bin"
	.incbin	"data/chr.smb/chr_bg_64.bin"
	.incbin	"data/chr.smb/chr_bg_65.bin"
	.incbin	"data/chr.smb/chr_bg_66.bin"
	.incbin	"data/chr.smb/chr_bg_67.bin"
	.incbin	"data/chr.smb/chr_bg_68.bin"
	.incbin	"data/chr.smb/chr_bg_69.bin"
	.incbin	"data/chr.smb/chr_bg_6A.bin"

.endif	; PIPE_V
