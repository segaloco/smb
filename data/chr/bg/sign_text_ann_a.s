.ifndef	SIGN_TEXT_ANN_A
SIGN_TEXT_ANN_A = 1

	.export chr_bg_sign_text_ann_a
chr_bg_sign_text_ann_a:
	.incbin	"data/chr.ann/chr_bg_EC.bin"
	.incbin	"data/chr.ann/chr_bg_ED.bin"
	.incbin	"data/chr.ann/chr_bg_EE.bin"
	.incbin	"data/chr.ann/chr_bg_EF.bin"
	.incbin	"data/chr.smb2/chr_bg_F0.bin"

.endif	; SIGN_TEXT_ANN_A
