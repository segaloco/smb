.ifndef	SIGN_TEXT_ANN_B
SIGN_TEXT_ANN_B = 1

	.export chr_bg_sign_text_ann_b
chr_bg_sign_text_ann_b:
	.incbin	"data/chr.ann/chr_bg_F3.bin"
	.incbin	"data/chr.ann/chr_bg_F4.bin"
	.incbin	"data/chr.ann/chr_bg_F5.bin"
	.incbin	"data/chr.ann/chr_bg_F6.bin"
	.incbin	"data/chr.ann/chr_bg_F7.bin"
	.incbin	"data/chr.ann/chr_bg_F8.bin"

.endif	; SIGN_TEXT_ANN_B
