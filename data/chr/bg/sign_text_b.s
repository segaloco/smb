.ifndef	SIGN_TEXT_B
SIGN_TEXT_B = 1

	.export chr_bg_sign_text_b
chr_bg_sign_text_b:
	.incbin	"data/chr.smb/chr_bg_D0.bin"
	.incbin	"data/chr.smb/chr_bg_D1.bin"
	.incbin	"data/chr.smb/chr_bg_D2.bin"
	.incbin	"data/chr.smb/chr_bg_D3.bin"
	.incbin	"data/chr.smb/chr_bg_D4.bin"
	.incbin	"data/chr.smb/chr_bg_D5.bin"
	.incbin	"data/chr.smb/chr_bg_D6.bin"
	.incbin	"data/chr.smb/chr_bg_D7.bin"
	.incbin	"data/chr.smb/chr_bg_D8.bin"
	.incbin	"data/chr.smb/chr_bg_D9.bin"
	.incbin	"data/chr.smb/chr_bg_DA.bin"
	.incbin	"data/chr.smb/chr_bg_DB.bin"
	.incbin	"data/chr.smb/chr_bg_DC.bin"
	.incbin	"data/chr.smb/chr_bg_DD.bin"
	.incbin	"data/chr.smb/chr_bg_DE.bin"
	.incbin	"data/chr.smb/chr_bg_DF.bin"
	.incbin	"data/chr.smb/chr_bg_E0.bin"
	.incbin	"data/chr.smb/chr_bg_E1.bin"
	.incbin	"data/chr.smb/chr_bg_E2.bin"
	.incbin	"data/chr.smb/chr_bg_E3.bin"
	.incbin	"data/chr.smb/chr_bg_E4.bin"
	.incbin	"data/chr.smb/chr_bg_E5.bin"
	.incbin	"data/chr.smb/chr_bg_E6.bin"
	.incbin	"data/chr.smb/chr_bg_E7.bin"
	.incbin	"data/chr.smb/chr_bg_E8.bin"

.endif	; SIGN_TEXT_B
