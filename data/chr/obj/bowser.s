.ifndef	KOOPA
KOOPA = 1

	.export chr_obj_bowser
chr_obj_bowser:
	.incbin	"../../chr.smb/chr_obj_BE.bin"
	.incbin	"../../chr.smb/chr_obj_BF.bin"
	.incbin	"../../chr.smb/chr_obj_C0.bin"
	.incbin	"../../chr.smb/chr_obj_C1.bin"
	.incbin	"../../chr.smb/chr_obj_C2.bin"
	.incbin	"../../chr.smb/chr_obj_C3.bin"
	.incbin	"../../chr.smb/chr_obj_C4.bin"
	.incbin	"../../chr.smb/chr_obj_C5.bin"
	.incbin	"../../chr.smb/chr_obj_C6.bin"
	.incbin	"../../chr.smb/chr_obj_C7.bin"
	.incbin	"../../chr.smb/chr_obj_C8.bin"
	.incbin	"../../chr.smb/chr_obj_C9.bin"
	.incbin	"../../chr.smb/chr_obj_CA.bin"
	.incbin	"../../chr.smb/chr_obj_CB.bin"
	.incbin	"../../chr.smb/chr_obj_CC.bin"

.endif	; KOOPA
