.ifndef	BUZZY
BUZZY = 1

	.export chr_obj_buzzy_a
chr_obj_buzzy_a:
	.incbin	"../../chr.smb/chr_obj_AA.bin"
	.incbin	"../../chr.smb/chr_obj_AB.bin"
	.incbin	"../../chr.smb/chr_obj_AC.bin"
	.incbin	"../../chr.smb/chr_obj_AD.bin"
	.incbin	"../../chr.smb/chr_obj_AE.bin"
	.incbin	"../../chr.smb/chr_obj_AF.bin"
	.incbin	"../../chr.smb/chr_obj_B0.bin"
	.incbin	"../../chr.smb/chr_obj_B1.bin"

.endif	; BUZZY
