.ifndef	TROOPA_A
TROOPA_A = 1

	.export chr_obj_koopa_a
chr_obj_koopa_a:
	.incbin	"../../chr.smb/chr_obj_69.bin"
	.incbin	"../../chr.smb/chr_obj_6A.bin"
	.incbin	"../../chr.smb/chr_obj_6B.bin"
	.incbin	"../../chr.smb/chr_obj_6C.bin"
	.incbin	"../../chr.smb/chr_obj_6D.bin"
	.incbin	"../../chr.smb/chr_obj_6E.bin"
	.incbin	"../../chr.smb/chr_obj_6F.bin"

.endif	; TROOPA_A
