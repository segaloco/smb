.ifndef	TROOPA_B
TROOPA_B = 1

	.export chr_obj_koopa_b
chr_obj_koopa_b:
	.incbin	"../../chr.smb/chr_obj_A0.bin"
	.incbin	"../../chr.smb/chr_obj_A1.bin"
	.incbin	"../../chr.smb/chr_obj_A2.bin"
	.incbin	"../../chr.smb/chr_obj_A3.bin"
	.incbin	"../../chr.smb/chr_obj_A4.bin"
	.incbin	"../../chr.smb/chr_obj_A5.bin"
	.incbin	"../../chr.smb/chr_obj_A6.bin"
	.incbin	"../../chr.smb/chr_obj_A7.bin"
	.incbin	"../../chr.smb/chr_obj_A8.bin"
	.incbin	"../../chr.smb/chr_obj_A9.bin"

.endif	; TROOPA_B
