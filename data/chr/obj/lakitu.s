.ifndef	LAKITU
LAKITU = 1

	.export chr_obj_lakitu
chr_obj_lakitu:
	.incbin	"../../chr.smb/chr_obj_B8.bin"
	.incbin	"../../chr.smb/chr_obj_B9.bin"
	.incbin	"../../chr.smb/chr_obj_BA.bin"
	.incbin	"../../chr.smb/chr_obj_BB.bin"
	.incbin	"../../chr.smb/chr_obj_BC.bin"
	.incbin	"../../chr.smb/chr_obj_BD.bin"

.endif	; LAKITU
