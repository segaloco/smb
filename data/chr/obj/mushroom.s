.ifndef	MUSHROOM
MUSHROOM = 1

	.export chr_obj_mushroom_nw
chr_obj_mushroom_nw:
	.incbin	"../../chr.smb/chr_obj_76.bin"

	.export chr_obj_mushroom_ne
chr_obj_mushroom_ne:
	.incbin	"../../chr.smb/chr_obj_77.bin"

	.export chr_obj_mushroom_sw
chr_obj_mushroom_sw:
	.incbin	"../../chr.smb/chr_obj_78.bin"

	.export chr_obj_mushroom_se
chr_obj_mushroom_se:
	.incbin	"../../chr.smb/chr_obj_79.bin"

.endif	; MUSHROOM
