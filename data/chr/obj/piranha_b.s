.ifndef	PIRANHA_B
PIRANHA_B = 1

	.export chr_obj_piranha_b
chr_obj_piranha_b:
	.incbin	"../../chr.smb/chr_obj_EB.bin"
	.incbin	"../../chr.smb/chr_obj_EC.bin"
	.incbin	"../../chr.smb/chr_obj_ED.bin"
	.incbin	"../../chr.smb/chr_obj_EE.bin"

.endif	; PIRANHA_B
