.ifndef	PLAYER_A
PLAYER_A = 1

	.export chr_obj_player_walk_large
chr_obj_player_walk_large:
	.incbin	"../../chr.smb/chr_obj_00.bin"
	.incbin	"../../chr.smb/chr_obj_01.bin"
	.incbin	"../../chr.smb/chr_obj_02.bin"
	.incbin	"../../chr.smb/chr_obj_03.bin"
	.incbin	"../../chr.smb/chr_obj_04.bin"
	.incbin	"../../chr.smb/chr_obj_05.bin"
	.incbin	"../../chr.smb/chr_obj_06.bin"
	.incbin	"../../chr.smb/chr_obj_07.bin"
	.incbin	"../../chr.smb/chr_obj_08.bin"
	.incbin	"../../chr.smb/chr_obj_09.bin"
	.incbin	"../../chr.smb/chr_obj_0A.bin"
	.incbin	"../../chr.smb/chr_obj_0B.bin"
	.incbin	"../../chr.smb/chr_obj_0C.bin"
	.incbin	"../../chr.smb/chr_obj_0D.bin"
	.incbin	"../../chr.smb/chr_obj_0E.bin"
	.incbin	"../../chr.smb/chr_obj_0F.bin"
	.incbin	"../../chr.smb/chr_obj_10.bin"
	.incbin	"../../chr.smb/chr_obj_11.bin"
	.incbin	"../../chr.smb/chr_obj_12.bin"
	.incbin	"../../chr.smb/chr_obj_13.bin"
	.incbin	"../../chr.smb/chr_obj_14.bin"
	.incbin	"../../chr.smb/chr_obj_15.bin"
	.incbin	"../../chr.smb/chr_obj_16.bin"
	.incbin	"../../chr.smb/chr_obj_17.bin"

	.export chr_obj_player_skid_large
chr_obj_player_skid_large:
	.incbin	"../../chr.smb/chr_obj_18.bin"
	.incbin	"../../chr.smb/chr_obj_19.bin"
	.incbin	"../../chr.smb/chr_obj_1A.bin"
	.incbin	"../../chr.smb/chr_obj_1B.bin"
	.incbin	"../../chr.smb/chr_obj_1C.bin"
	.incbin	"../../chr.smb/chr_obj_1D.bin"
	.incbin	"../../chr.smb/chr_obj_1E.bin"
	.incbin	"../../chr.smb/chr_obj_1F.bin"

	.export chr_obj_player_jump_large
chr_obj_player_jump_large:
	.incbin	"../../chr.smb/chr_obj_20.bin"
	.incbin	"../../chr.smb/chr_obj_21.bin"
	.incbin	"../../chr.smb/chr_obj_22.bin"
	.incbin	"../../chr.smb/chr_obj_23.bin"
	.incbin	"../../chr.smb/chr_obj_24.bin"
	.incbin	"../../chr.smb/chr_obj_25.bin"
	.incbin	"../../chr.smb/chr_obj_26.bin"
	.incbin	"../../chr.smb/chr_obj_27.bin"

	.export chr_obj_player_swim_large
chr_obj_player_swim_large:
	.incbin	"../../chr.smb/chr_obj_28.bin"
	.incbin	"../../chr.smb/chr_obj_29.bin"
	.incbin	"../../chr.smb/chr_obj_2A.bin"
	.incbin	"../../chr.smb/chr_obj_2B.bin"
	.incbin	"../../chr.smb/chr_obj_2C.bin"
	.incbin	"../../chr.smb/chr_obj_2D.bin"
	.incbin	"../../chr.smb/chr_obj_2E.bin"
	.incbin	"../../chr.smb/chr_obj_2F.bin"
	.incbin	"../../chr.smb/chr_obj_30.bin"
	.incbin	"../../chr.smb/chr_obj_31.bin"

	.export chr_obj_player_walk_small
chr_obj_player_walk_small:
	.incbin	"../../chr.smb/chr_obj_32.bin"
	.incbin	"../../chr.smb/chr_obj_33.bin"
	.incbin	"../../chr.smb/chr_obj_34.bin"
	.incbin	"../../chr.smb/chr_obj_35.bin"
	.incbin	"../../chr.smb/chr_obj_36.bin"
	.incbin	"../../chr.smb/chr_obj_37.bin"
	.incbin	"../../chr.smb/chr_obj_38.bin"
	.incbin	"../../chr.smb/chr_obj_39.bin"
	.incbin	"../../chr.smb/chr_obj_3A.bin"
	.incbin	"../../chr.smb/chr_obj_3B.bin"
	.incbin	"../../chr.smb/chr_obj_3C.bin"

	.export chr_obj_player_skid_small
chr_obj_player_skid_small:
	.incbin	"../../chr.smb/chr_obj_3D.bin"
	.incbin	"../../chr.smb/chr_obj_3E.bin"
	.incbin	"../../chr.smb/chr_obj_3F.bin"
	.incbin	"../../chr.smb/chr_obj_40.bin"

	.export chr_obj_player_jump_small
chr_obj_player_jump_small:
	.incbin	"../../chr.smb/chr_obj_41.bin"
	.incbin	"../../chr.smb/chr_obj_42.bin"
	.incbin	"../../chr.smb/chr_obj_43.bin"

	.export chr_obj_player_swim_small
chr_obj_player_swim_small:
	.incbin	"../../chr.smb/chr_obj_44.bin"
	.incbin	"../../chr.smb/chr_obj_45.bin"
	.incbin	"../../chr.smb/chr_obj_46.bin"
	.incbin	"../../chr.smb/chr_obj_47.bin"
	.incbin	"../../chr.smb/chr_obj_48.bin"
	.incbin	"../../chr.smb/chr_obj_49.bin"

	.export chr_obj_player_resize
chr_obj_player_resize:
	.incbin	"../../chr.smb/chr_obj_4A.bin"
	.incbin	"../../chr.smb/chr_obj_4B.bin"
	.incbin	"../../chr.smb/chr_obj_4C.bin"
	.incbin	"../../chr.smb/chr_obj_4D.bin"
	.incbin	"../../chr.smb/chr_obj_4E.bin"
	.incbin	"../../chr.smb/chr_obj_4F.bin"

.endif	; PLAYER_A
