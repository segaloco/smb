.ifndef	SPINY
SPINY = 1

	.export chr_obj_spiny
chr_obj_spiny:
	.incbin	"../../chr.smb/chr_obj_96.bin"
	.incbin	"../../chr.smb/chr_obj_97.bin"
	.incbin	"../../chr.smb/chr_obj_98.bin"
	.incbin	"../../chr.smb/chr_obj_99.bin"
	.incbin	"../../chr.smb/chr_obj_9A.bin"
	.incbin	"../../chr.smb/chr_obj_9B.bin"
	.incbin	"../../chr.smb/chr_obj_9C.bin"
	.incbin	"../../chr.smb/chr_obj_9D.bin"

.endif	; SPINY
