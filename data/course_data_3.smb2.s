.include	"course_flags.i"

.segment	"DATA3"

.if	.defined(OG_PAD)
	.res	106, $FF
.endif

	.include	"./courses/smb2/castle_9_actor.s"
	.include	"./courses/smb2/castle_10_actor.s"
; --------------
	.include	"./courses/smb2/overworld_25_actor.s"
	.include	"./courses/smb2/overworld_26_actor.s"
	.include	"./courses/smb2/overworld_27_actor.s"
; --------------
	.include	"./courses/smb2/water_6_actor.s"
	.include	"./courses/smb2/water_7_actor.s"
	.include	"./courses/smb2/water_8_actor.s"
; ------------------------------
	.include	"./courses/smb2/castle_9_scenery.s"
	.include	"./courses/smb2/castle_10_scenery.s"
; --------------
	.include	"./courses/smb2/overworld_25_scenery.s"
	.include	"./courses/smb2/overworld_26_scenery.s"
	.include	"./courses/smb2/overworld_27_scenery.s"
; --------------
	.include	"./courses/smb2/water_6_scenery.s"
	.include	"./courses/smb2/water_7_scenery.s"
	.include	"./courses/smb2/water_8_scenery.s"
