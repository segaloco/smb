.include "course_flags.i"

	.export	course_ext_actor_overworld_12
course_ext_actor_overworld_12:
	.word	(COURSE_ACTORS_POS_Y_A|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PLAT_STD_H_B)
	.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_01)
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_1|COURSE_ACTORS_TYPE_E_STARTPAGE_03)

	.if	.defined(SMB2)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_03)
		.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_2|COURSE_ACTORS_TYPE_E_STARTPAGE_07)
	.elseif	.defined(ANN)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_12_SMB2)
		.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_2|COURSE_ACTORS_TYPE_E_STARTPAGE_10)
	.endif

	.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_2|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_05)
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_3|COURSE_ACTORS_TYPE_E_STARTPAGE_08)

	.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_2|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_09)
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_4|COURSE_ACTORS_TYPE_E_STARTPAGE_07)

	.byte	COURSE_ACTORS_END
