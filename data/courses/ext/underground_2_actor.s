.include	"course_flags.i"

	.export	course_ext_actor_underground_2
course_ext_actor_underground_2:
	.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_01)
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_1|COURSE_ACTORS_TYPE_E_STARTPAGE_09)

	.if	.defined(SMB2)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_08)
		.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_4|COURSE_ACTORS_TYPE_E_STARTPAGE_07)
	.elseif	.defined(ANN)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_19_SMB2)
		.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_4|COURSE_ACTORS_TYPE_E_STARTPAGE_01)
	.endif


	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_03)
	.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_09)
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_4|COURSE_ACTORS_TYPE_E_STARTPAGE_08)

	.if	.defined(ANN)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_12_SMB2)
		.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_2|COURSE_ACTORS_TYPE_E_STARTPAGE_07)
	.endif

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_05)
	.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_05)
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_3|COURSE_ACTORS_TYPE_E_STARTPAGE_08)

	.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_SUBSPACE_CASTLE|COURSE_EXT_ACTORS_SUBSPACE_CASTLE_04)
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_4|COURSE_ACTORS_TYPE_E_STARTPAGE_08)

	.byte	COURSE_ACTORS_END
