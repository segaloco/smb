.include	"course_flags.i"

	.export course_actor_castle_3
course_actor_castle_3:
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PODOBOO)
	.if	.defined(SMB)
		.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_4|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_PODOBOO)
		.word	(COURSE_ACTORS_POS_Y_7|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_FIREBAR_LONG)
	.elseif	.defined(VS_SMB)
		.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_4|COURSE_ACTORS_ID_PODOBOO)
		.word	(COURSE_ACTORS_POS_Y_7|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_ID_FIREBAR_LONG)
	.elseif	.defined(ANN)
		.word	(COURSE_ACTORS_POS_Y_7|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_ID_FIREBAR_CLOCK_SLOW)
	.endif
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_E|COURSE_ACTORS_ID_PODOBOO)

	.if	.defined(SMB)
		.word	(COURSE_ACTORS_POS_Y_D|COURSE_ACTORS_POS_X_B|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_FIREBAR_CLOCK_SLOW)
	.elseif	.defined(VS_SMB)
		.word	(COURSE_ACTORS_POS_Y_D|COURSE_ACTORS_POS_X_B|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_FIREBAR_CLOCK_SLOW)
	.endif

	.if	.defined(SMBV1)
		.word	(COURSE_ACTORS_POS_Y_9|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)
	.elseif	.defined(ANN)
		.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_03)
		.word	(COURSE_ACTORS_POS_Y_9|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)
	.endif
	.word	(COURSE_ACTORS_POS_Y_5|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)

	.if	.defined(SMBV1)
		.word	(COURSE_ACTORS_POS_Y_D|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_FIREBAR_CLOCK_SLOW)
	.endif

	.word	(COURSE_ACTORS_POS_Y_9|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)

	.if	.defined(SMBV1)
		.word	(COURSE_ACTORS_POS_Y_D|COURSE_ACTORS_POS_X_3|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)
	.endif

	.if	.defined(SMBV1)
		.word	(COURSE_ACTORS_POS_Y_9|COURSE_ACTORS_POS_X_9|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)
	.elseif	.defined(ANN)
		.word	(COURSE_ACTORS_POS_Y_9|COURSE_ACTORS_POS_X_9|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)
	.endif

	.word	(COURSE_ACTORS_POS_Y_6|COURSE_ACTORS_POS_X_2|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)
	.word	(COURSE_ACTORS_POS_Y_A|COURSE_ACTORS_POS_X_5|COURSE_ACTORS_ID_PLAT_S_UP)
	.word	(COURSE_ACTORS_POS_Y_A|COURSE_ACTORS_POS_X_8|COURSE_ACTORS_ID_PLAT_S_DOWN)
	.word	(COURSE_ACTORS_POS_Y_A|COURSE_ACTORS_POS_X_C|COURSE_ACTORS_ID_FIREBAR_CLOCK_SLOW)

	.word	(COURSE_ACTORS_POS_Y_0|COURSE_ACTORS_POS_X_2|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_BOWSER_FLAME)

	.if	.defined(SMBV1)
		.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_FIREBAR_CLOCK_FAST)
		.if	.defined(SMB)
			.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_PODOBOO)
		.elseif	.defined(VS_SMB)
			.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_ID_PODOBOO)
		.endif

		.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_PODOBOO)

		.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_3|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_PODOBOO)
	.elseif	.defined(ANN)
		.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_08)
	.endif

	.word	(COURSE_ACTORS_POS_Y_8|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_ID_BOWSER)
	.word	(COURSE_ACTORS_POS_Y_6|COURSE_ACTORS_POS_X_A|COURSE_ACTORS_ID_PLAT_STD_H)

	.word	(COURSE_ACTORS_POS_Y_0|COURSE_ACTORS_POS_X_9|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_TOAD)
	.byte	COURSE_ACTORS_END
