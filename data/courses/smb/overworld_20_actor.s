.include	"course_flags.i"

	.export course_actor_overworld_20
course_actor_overworld_20:
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_A|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_PARA_GREEN_J)
	.if	.defined(SMBV1)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_SUBSPACE_UNDERGROUND|COURSE_ACTORS_SUBSPACE_UNDERGROUND_03)
	.elseif	.defined(ANN)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_SUBSPACE_UNDERGROUND|COURSE_ACTORS_SUBSPACE_UNDERGROUND_04_ANN)
	.endif
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_7|COURSE_ACTORS_TYPE_E_STARTPAGE_00)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_C|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_PARA_GREEN_J)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_5|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PARA_GREEN_J)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_PARA_GREEN_J)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_4|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_HAMMER_BRO)
	.word	(COURSE_ACTORS_POS_Y_7|COURSE_ACTORS_POS_X_6|COURSE_ACTORS_HARD_ONLY|COURSE_ACTORS_ID_HAMMER_BRO)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_07)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_2|COURSE_ACTORS_ID_KOOPA_GREEN)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_HAMMER_BRO)
	.word	(COURSE_ACTORS_POS_Y_7|COURSE_ACTORS_POS_X_9|COURSE_ACTORS_ID_HAMMER_BRO)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_0A)

	.if	.defined(VS_SMB)
		.word	(COURSE_ACTORS_POS_Y_4|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_ID_GOOMBA)
		.word	(COURSE_ACTORS_POS_Y_2|COURSE_ACTORS_POS_X_8|COURSE_ACTORS_ID_GOOMBA)
	.endif

	.if	.defined(SMBM)
		.word	(COURSE_ACTORS_POS_Y_2|COURSE_ACTORS_POS_X_9|COURSE_ACTORS_ID_BUZZY_BEETLE)
	.elseif	.defined(VS_SMB)
		.word	(COURSE_ACTORS_POS_Y_2|COURSE_ACTORS_POS_X_9|COURSE_ACTORS_ID_GOOMBA)
	.endif
	.byte	COURSE_ACTORS_END
