.include	"course_flags.i"

	.export course_actor_overworld_22
course_actor_overworld_22:
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_KOOPA_GREEN)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_B|COURSE_ACTORS_ID_GOOMBA_TRIO_LO)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_4|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_KOOPA_TRIO_LO)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_E|COURSE_ACTORS_ID_KOOPA_DUO_LO)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_04)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_2|COURSE_ACTORS_ID_KOOPA_GREEN)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_A|COURSE_ACTORS_ID_GOOMBA_TRIO_LO)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_E|COURSE_ACTORS_ID_KOOPA_GREEN)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_C|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PARA_GREEN_J)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_F|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_KOOPA_GREEN)

	.if	.defined(SMBV1)
		.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_A|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_GOOMBA_TRIO_LO)
	.elseif	.defined(ANN)
		.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_9|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_GOOMBA_TRIO_LO)
	.endif

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_6|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_KOOPA_GREEN)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_F|COURSE_ACTORS_ID_KOOPA_TRIO_LO)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_9|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_KOOPA_DUO_LO)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_5|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_KOOPA_TRIO_LO)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_F|COURSE_ACTORS_ID_KOOPA_GREEN)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_6|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_GOOMBA_TRIO_LO)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_F|COURSE_ACTORS_ID_GOOMBA_TRIO_LO)

.if	.defined(ANN)
	.export course_actor_overworld_22_end
course_actor_overworld_22_end:
.endif
	.byte	COURSE_ACTORS_END
