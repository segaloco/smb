.include	"course_flags.i"

	.export course_actor_overworld_2
course_actor_overworld_2:
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_E|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PARA_GREEN_J)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_03)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_F|COURSE_ACTORS_ID_HAMMER_BRO)

	.word	(COURSE_ACTORS_POS_Y_7|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_HAMMER_BRO)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PARA_GREEN_J)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_07)
	.word	(COURSE_ACTORS_POS_Y_7|COURSE_ACTORS_POS_X_5|COURSE_ACTORS_ID_HAMMER_BRO)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_ID_HAMMER_BRO)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_9|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_KOOPA_GREEN)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_2|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_HAMMER_BRO)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_F|COURSE_ACTORS_ID_HAMMER_BRO)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_0B)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_ID_HAMMER_BRO)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_9|COURSE_ACTORS_ID_HAMMER_BRO)
	.byte	COURSE_ACTORS_END
