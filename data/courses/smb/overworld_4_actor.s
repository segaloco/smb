.include	"course_flags.i"

	.export course_actor_overworld_4
course_actor_overworld_4:
	.if	.defined(SMBV1)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_SUBSPACE_UNDERGROUND|COURSE_ACTORS_SUBSPACE_UNDERGROUND_03)
		.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_6|COURSE_ACTORS_TYPE_E_STARTPAGE_08)
	.elseif	.defined(ANN)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_SUBSPACE_UNDERGROUND|COURSE_ACTORS_SUBSPACE_UNDERGROUND_04_ANN)
		.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_6|COURSE_ACTORS_TYPE_E_STARTPAGE_06)
	.endif

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_A|COURSE_ACTORS_ID_KOOPA_GREEN)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_B|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PARA_GREEN_J)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_6|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_BUZZY_BEETLE)
	.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_SUBSPACE_WATER|COURSE_ACTORS_SUBSPACE_WATER_01)
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_6|COURSE_ACTORS_TYPE_E_STARTPAGE_00)


	.word	(COURSE_ACTORS_POS_Y_3|COURSE_ACTORS_POS_X_3|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_GOOMBA)
	.word	(COURSE_ACTORS_POS_Y_3|COURSE_ACTORS_POS_X_4|COURSE_ACTORS_ID_GOOMBA)

	.if	.defined(SMBV1)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_3|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_ACTORS_SUBSPACE_OVERWORLD_21)
	.elseif	.defined(ANN)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_3|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_ACTORS_SUBSPACE_OVERWORLD_27_ANN)
	.endif
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_6|COURSE_ACTORS_TYPE_E_STARTPAGE_00)

	.if	.defined(VS_SMB)
		.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_3|COURSE_ACTORS_ID_KOOPA_GREEN)
	.endif

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_C|COURSE_ACTORS_ID_BUZZY_BEETLE)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_07)
	.if	.defined(SMBV1)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_SUBSPACE_UNDERGROUND|COURSE_ACTORS_SUBSPACE_UNDERGROUND_03)
		.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_6|COURSE_ACTORS_TYPE_E_STARTPAGE_06)
	.elseif	.defined(ANN)
		.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_SUBSPACE_UNDERGROUND|COURSE_ACTORS_SUBSPACE_UNDERGROUND_04_ANN)
		.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_6|COURSE_ACTORS_TYPE_E_STARTPAGE_04)
	.endif

	.word	(COURSE_ACTORS_POS_Y_3|COURSE_ACTORS_POS_X_8|COURSE_ACTORS_ID_BUZZY_BEETLE)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_0A)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_3|COURSE_ACTORS_ID_BUZZY_BEETLE)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_C|COURSE_ACTORS_ID_GOOMBA_DUO_LO)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_0C)

	.if	.defined(SMBM)
		.word	(COURSE_ACTORS_POS_Y_3|COURSE_ACTORS_POS_X_E|COURSE_ACTORS_ID_PARA_GREEN_J)
	.elseif	.defined(VS_SMB)
		.word	(COURSE_ACTORS_POS_Y_6|COURSE_ACTORS_POS_X_A|COURSE_ACTORS_ID_GOOMBA)
		.word	(COURSE_ACTORS_POS_Y_4|COURSE_ACTORS_POS_X_C|COURSE_ACTORS_ID_GOOMBA)
		.word	(COURSE_ACTORS_POS_Y_2|COURSE_ACTORS_POS_X_E|COURSE_ACTORS_ID_GOOMBA)
	.endif
	.byte	COURSE_ACTORS_END
