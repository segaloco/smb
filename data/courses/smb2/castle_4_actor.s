.include	"course_flags.i"

	.export course_actor_castle_4_smb2
course_actor_castle_4_smb2:
	.word	(COURSE_ACTORS_POS_Y_9|COURSE_ACTORS_POS_X_4|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_FIREBAR_LONG)
	.word	(COURSE_ACTORS_POS_Y_7|COURSE_ACTORS_POS_X_6|COURSE_ACTORS_ID_KOOPA_RED)

	.word	(COURSE_ACTORS_POS_Y_9|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)
	.word	(COURSE_ACTORS_POS_Y_0|COURSE_ACTORS_POS_X_A|COURSE_ACTORS_ID_GOOMBA_TRIO_HI)

	.word	(COURSE_ACTORS_POS_Y_7|COURSE_ACTORS_POS_X_5|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_FIREBAR_LONG)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_B|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)
	.word	(COURSE_ACTORS_POS_Y_5|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_ID_PLAT_STD_V)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_05)
	.word	(COURSE_ACTORS_POS_Y_8|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)
	.word	(COURSE_ACTORS_POS_Y_4|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_ID_KOOPA_GREEN)
	.word	(COURSE_ACTORS_POS_Y_4|COURSE_ACTORS_POS_X_8|COURSE_ACTORS_ID_KOOPA_GREEN)
	.word	(COURSE_ACTORS_POS_Y_4|COURSE_ACTORS_POS_X_9|COURSE_ACTORS_ID_KOOPA_GREEN)
	.word	(COURSE_ACTORS_POS_Y_6|COURSE_ACTORS_POS_X_C|COURSE_ACTORS_ID_PLAT_DROP)

	.word	(COURSE_ACTORS_POS_Y_9|COURSE_ACTORS_POS_X_4|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_ID_HAMMER_BRO)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_08)

	.if	.defined(SMB2)
		.word	(COURSE_ACTORS_POS_Y_5|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_FIREBAR_CLOCK_SLOW)
	.elseif	.defined(ANN)
		.word	(COURSE_ACTORS_POS_Y_5|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_ID_FIREBAR_CLOCK_SLOW)
	.endif
	.word	(COURSE_ACTORS_POS_Y_9|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW)
	.word	(COURSE_ACTORS_POS_Y_0|COURSE_ACTORS_POS_X_B|COURSE_ACTORS_ID_GOOMBA_TRIO_LO)

	.word	(COURSE_ACTORS_POS_Y_0|COURSE_ACTORS_POS_X_8|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_BOWSER_FLAME)
	.word	(COURSE_ACTORS_POS_Y_0|COURSE_ACTORS_POS_X_C|COURSE_ACTORS_ID_KOOPA_TRIO_LO)

	.word	(COURSE_ACTORS_POS_Y_C|COURSE_ACTORS_POS_X_E|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PLAT_STD_H)

	.word	(COURSE_ACTORS_POS_Y_C|COURSE_ACTORS_POS_X_C|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PODOBOO)

	.word	(COURSE_ACTORS_POS_Y_A|COURSE_ACTORS_POS_X_4|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_FIREBAR_CLOCK_SLOW)
	.word	(COURSE_ACTORS_POS_Y_8|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_ID_BOWSER)

	.word	(COURSE_ACTORS_POS_Y_0|COURSE_ACTORS_POS_X_9|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_TOAD)
	.byte	COURSE_ACTORS_END
