.include	"course_flags.i"

	.export course_actor_overworld_23_smb2
course_actor_overworld_23_smb2:
	.word	(COURSE_ACTORS_POS_Y_9|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PLAT_DROP)
	.word	(COURSE_ACTORS_POS_Y_6|COURSE_ACTORS_POS_X_8|COURSE_ACTORS_ID_LAKITU)
	.word	(COURSE_ACTORS_POS_Y_5|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_ID_PARA_GREEN_F)

	.word	(COURSE_ACTORS_POS_Y_3|COURSE_ACTORS_POS_X_A|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PARA_RED_F)
	.word	(COURSE_ACTORS_POS_Y_5|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_ID_PLAT_DROP)

	.word	(COURSE_ACTORS_POS_Y_6|COURSE_ACTORS_POS_X_8|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_LAKITU)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_2|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_KOOPA_RED)
	.word	(COURSE_ACTORS_POS_Y_8|COURSE_ACTORS_POS_X_5|COURSE_ACTORS_ID_KOOPA_RED)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_5|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_HAMMER_BRO)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_E|COURSE_ACTORS_ID_HAMMER_BRO)

	.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_3|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_ACTORS_SUBSPACE_OVERWORLD_29)
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_8|COURSE_ACTORS_TYPE_E_STARTPAGE_00)


	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_09)
	.word	(COURSE_ACTORS_POS_Y_3|COURSE_ACTORS_POS_X_4|COURSE_ACTORS_ID_PARA_RED_F)
	.word	(COURSE_ACTORS_POS_Y_4|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_ID_PARA_RED_F)

	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_6|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_HAMMER_BRO)
	.word	(COURSE_ACTORS_POS_Y_B|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_ID_HAMMER_BRO)

	.word	(COURSE_ACTORS_POS_Y_6|COURSE_ACTORS_POS_X_C|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PLAT_BAL)

	.word	(COURSE_ACTORS_POS_Y_9|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PLAT_BAL)

	.word	(COURSE_ACTORS_POS_Y_2|COURSE_ACTORS_POS_X_1|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_PARA_RED_F)

	.export course_actor_overworld_24_smb2
course_actor_overworld_24_smb2:
	.byte	COURSE_ACTORS_END
