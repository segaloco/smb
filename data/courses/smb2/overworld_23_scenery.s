.include	"course_flags.i"

	.export course_scenery_overworld_23_smb2
course_scenery_overworld_23_smb2:
	.word	(COURSE_AREA_LEDGE_MUSHROOM|COURSE_BG_CLOUD|COURSE_FLOOR_MASK_0|COURSE_TIMER_SETTING_1|COURSE_SCENERY_ENTER_POS_B0_A|COURSE_SCENERY_FG_NONE)

	.word	(COURSE_SCENERY_TYPE_F|COURSE_SCENERY_POS_X_0|COURSE_OBJ_B_CASTLE|COURSE_OBJ_DIM_7)
	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_0|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_16)

	.word	(COURSE_SCENERY_POS_Y_7|COURSE_SCENERY_POS_X_5|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_3)
	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_8|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_3)
	.word	(COURSE_SCENERY_POS_Y_2|COURSE_SCENERY_POS_X_D|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_5)

	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_4|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_3)
	.word	(COURSE_SCENERY_POS_Y_9|COURSE_SCENERY_POS_X_5|COURSE_OBJ_C_SPRING)

	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_0|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_6)
	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_B|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_16)
	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_B|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_WALLS)<<8)

	.word	(COURSE_SCENERY_POS_Y_8|COURSE_SCENERY_POS_X_5|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_FLOORCOL|COURSE_OBJ_DIM_3)
	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_9|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_NONE)<<8)
	.word	(COURSE_SCENERY_POS_Y_7|COURSE_SCENERY_POS_X_9|COURSE_OBJ_C_BLOCK_ITEM_A)
	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_B|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_16)

	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_A|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_WALLS)<<8)
	.word	(COURSE_SCENERY_POS_Y_6|COURSE_SCENERY_POS_X_B|COURSE_OBJ_C_BLOCK_ITEM_A)
	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_B|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_16)
	.word	(COURSE_SCENERY_TYPE_D|COURSE_SCENERY_POS_X_D|COURSE_SCENERY_TYPE_D_MASK|COURSE_OBJ_D_SWARM_C)
	.word	(COURSE_SCENERY_POS_Y_6|COURSE_SCENERY_POS_X_F|COURSE_OBJ_C_BLOCK_ITEM_E)

	.word	(COURSE_SCENERY_POS_Y_6|COURSE_SCENERY_POS_X_2|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_C_BLOCK_ITEM_B)
	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_8|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_NONE)<<8)
	.word	(COURSE_SCENERY_POS_Y_8|COURSE_SCENERY_POS_X_9|COURSE_OBJ_FLOORCOL|COURSE_OBJ_DIM_3)
	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_E|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_2)

	.word	(COURSE_SCENERY_POS_Y_7|COURSE_SCENERY_POS_X_0|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_PIPE_VERT_B|COURSE_OBJ_DIM_4)
	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_0|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_14)
	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_2|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_WALLS)<<8)
	.word	(COURSE_SCENERY_POS_Y_7|COURSE_SCENERY_POS_X_4|COURSE_OBJ_C_BLOCK_ITEM_C)
	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_C|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_NONE)<<8)
	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_E|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_16)
	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_E|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_WALLS)<<8)

	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_5|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_NONE)<<8)
	.word	(COURSE_SCENERY_POS_Y_9|COURSE_SCENERY_POS_X_D|COURSE_OBJ_C_SPRING)

	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_A|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_16)
	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_B|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_WALLS)<<8)

	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_8|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_NONE)<<8)
	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_A|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_14)
	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_A|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_WALLS)<<8)

	.word	(COURSE_SCENERY_POS_Y_6|COURSE_SCENERY_POS_X_3|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_C_BLOCK_COIN)
	.word	(COURSE_SCENERY_POS_Y_6|COURSE_SCENERY_POS_X_5|COURSE_OBJ_C_BLOCK_ITEM_A)
	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_5|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_NONE)<<8)
	.word	(COURSE_SCENERY_TYPE_C|COURSE_SCENERY_POS_X_D|COURSE_OBJ_A_PULLEY|COURSE_OBJ_DIM_6)
	.word	(COURSE_SCENERY_TYPE_F|COURSE_SCENERY_POS_X_D|COURSE_OBJ_B_ROPE_PLAT|COURSE_OBJ_DIM_3)

	.word	(COURSE_SCENERY_TYPE_F|COURSE_SCENERY_POS_X_2|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_B_ROPE_PLAT|COURSE_OBJ_DIM_6)
	.word	(COURSE_SCENERY_POS_Y_3|COURSE_SCENERY_POS_X_C|COURSE_OBJ_FLOORROW|COURSE_OBJ_DIM_2)

	.word	(COURSE_SCENERY_POS_Y_B|COURSE_SCENERY_POS_X_5|COURSE_SCENERY_NEW_PAGE|COURSE_OBJ_LEDGE|COURSE_OBJ_DIM_16)
	.word	(COURSE_SCENERY_TYPE_D|COURSE_SCENERY_POS_X_6|COURSE_SCENERY_TYPE_D_MASK|COURSE_OBJ_D_POLE)
	.word	(COURSE_SCENERY_TYPE_E|COURSE_SCENERY_POS_X_8|COURSE_OBJ_E_FLOOR_MASK|COURSE_OBJ_E_MASK_UNK_B|(COURSE_SCENERY_FG_WALLS)<<8)
	.word	(COURSE_SCENERY_TYPE_F|COURSE_SCENERY_POS_X_A|COURSE_OBJ_B_CASTLE|COURSE_OBJ_DIM_1)

	.word	(COURSE_SCENERY_TYPE_D|COURSE_SCENERY_POS_X_A|COURSE_SCENERY_NEW_PAGE|COURSE_SCENERY_TYPE_D_MASK|COURSE_OBJ_D_LOCK_START)

	.export course_scenery_overworld_24_smb2
course_scenery_overworld_24_smb2:
	.byte	COURSE_SCENERY_END
