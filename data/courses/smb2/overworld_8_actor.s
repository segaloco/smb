.include	"course_flags.i"

	.export	course_actor_overworld_8_smb2
course_actor_overworld_8_smb2:
	.word	(COURSE_ACTORS_POS_Y_2|COURSE_ACTORS_POS_X_E|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_LAKITU)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_03)
	.word	(COURSE_ACTORS_POS_Y_2|COURSE_ACTORS_POS_X_4|COURSE_ACTORS_ID_LAKITU)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_06)
	.word	(COURSE_ACTORS_POS_Y_2|COURSE_ACTORS_POS_X_7|COURSE_ACTORS_ID_LAKITU)

	.word	(COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_PAGE_LOC_08)
	.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_E|COURSE_ACTORS_SUBSPACE_WATER|COURSE_ACTORS_SUBSPACE_WATER_03)
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_4|COURSE_ACTORS_TYPE_E_STARTPAGE_00)

	.word	(COURSE_ACTORS_POS_Y_2|COURSE_ACTORS_POS_X_0|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_LAKITU)

	.word	(COURSE_ACTORS_TYPE_E|COURSE_ACTORS_POS_X_E|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_SUBSPACE_OVERWORLD|COURSE_ACTORS_SUBSPACE_OVERWORLD_20)
	.byte	(COURSE_ACTORS_TYPE_E_COURSE_NO_4|COURSE_ACTORS_TYPE_E_STARTPAGE_00)

	.word	(COURSE_ACTORS_POS_Y_3|COURSE_ACTORS_POS_X_D|COURSE_ACTORS_NEW_PAGE|COURSE_ACTORS_ID_GOOMBA)
	.byte	COURSE_ACTORS_END

