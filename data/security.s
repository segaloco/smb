.include	"system/fds_charmap.i"

.segment	"KYODAKUDATA"

	.export	kyodaku
kyodaku:
        .byte   "           NINTENDO r           "
        .byte   "       FAMILY COMPUTER TM       "
        .byte   "                                "
        .byte   "  THIS PRODUCT IS MANUFACTURED  "
        .byte   "  AND SOLD BY NINTENDO CO,LTD.  "
        .byte   "  OR BY OTHER COMPANY UNDER     "
        .byte   "  LICENSE OF NINTENDO CO,LTD..  "
