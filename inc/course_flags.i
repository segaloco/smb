.ifndef	COURSE_FLAGS_I
COURSE_FLAGS_I = 1

.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"misc.i"
.include	"course_ids.i"

COURSE_AREA_OFFSET_MAX	= $1F

COURSE_SCENERY_FG_BG_SHIFT	= 0
COURSE_SCENERY_FG_NONE_VAL	= 0
COURSE_SCENERY_FG_NONE		= (COURSE_SCENERY_FG_NONE_VAL<<COURSE_SCENERY_FG_BG_SHIFT)
COURSE_SCENERY_FG_WATER_VAL	= (fg_scenery_tbl_water-fg_scenery_tbl+1)
COURSE_SCENERY_FG_WATER		= (COURSE_SCENERY_FG_WATER_VAL<<COURSE_SCENERY_FG_BG_SHIFT)
COURSE_SCENERY_FG_WALLS_VAL	= (fg_scenery_tbl_walls-fg_scenery_tbl+1)
COURSE_SCENERY_FG_WALLS		= (COURSE_SCENERY_FG_WALLS_VAL<<COURSE_SCENERY_FG_BG_SHIFT)
COURSE_SCENERY_FG_SURFACE_VAL	= (fg_scenery_tbl_surface-fg_scenery_tbl+1)
COURSE_SCENERY_FG_SURFACE	= (COURSE_SCENERY_FG_SURFACE_VAL<<COURSE_SCENERY_FG_BG_SHIFT)
COURSE_SCENERY_FG_MAX		= <(COURSE_SCENERY_FG_SURFACE>>COURSE_SCENERY_FG_BG_SHIFT)

COURSE_SCENERY_BG_COL_NONE_VAL	= ((0)/PPU_COLOR_ROW_SIZE)
COURSE_SCENERY_BG_COL_NONE	= ((COURSE_SCENERY_BG_COL_NONE_VAL|4)<<COURSE_SCENERY_FG_BG_SHIFT)
COURSE_SCENERY_BG_COL_WATER_VAL	= (((bg_color_data_water-bg_color_data)/PPU_COLOR_ROW_SIZE)+1)
COURSE_SCENERY_BG_COL_WATER	= ((COURSE_SCENERY_BG_COL_WATER_VAL|4)<<COURSE_SCENERY_FG_BG_SHIFT)
COURSE_SCENERY_BG_COL_OVERWORLD_VAL = (((bg_color_data_overworld-bg_color_data)/PPU_COLOR_ROW_SIZE)+1)
COURSE_SCENERY_BG_COL_OVERWORLD	= ((COURSE_SCENERY_BG_COL_OVERWORLD_VAL|4)<<COURSE_SCENERY_FG_BG_SHIFT)
COURSE_SCENERY_BG_COL_UNDERGROUND_VAL = (((bg_color_data_underground-bg_color_data)/PPU_COLOR_ROW_SIZE)+1)
COURSE_SCENERY_BG_COL_UNDERGROUND = ((COURSE_SCENERY_BG_COL_UNDERGROUND_VAL|4)<<COURSE_SCENERY_FG_BG_SHIFT)
COURSE_SCENERY_BG_COL_MAX	= <(COURSE_SCENERY_BG_COL_UNDERGROUND>>COURSE_SCENERY_FG_BG_SHIFT)
COURSE_SCENERY_BG_FG_MASK	= 7

COURSE_SCENERY_ENTER_POS_SHIFT	= 3
COURSE_SCENERY_ENTER_POS_00_A_VAL	= <(player_start_y_00_A-player_start_y)
COURSE_SCENERY_ENTER_POS_20_VAL		= <(player_start_y_20-player_start_y)
COURSE_SCENERY_ENTER_POS_B0_A_VAL	= <(player_start_y_B0_A-player_start_y)
COURSE_SCENERY_ENTER_POS_50_VAL		= <(player_start_y_50-player_start_y)
COURSE_SCENERY_ENTER_POS_00_B_VAL	= <(player_start_y_00_B-player_start_y)
COURSE_SCENERY_ENTER_POS_00_C_VAL	= <(player_start_y_00_C-player_start_y)
COURSE_SCENERY_ENTER_POS_B0_B_VAL	= <(player_start_y_B0_B-player_start_y)
COURSE_SCENERY_ENTER_POS_B0_C_VAL	= <(player_start_y_B0_C-player_start_y)
COURSE_SCENERY_ENTER_POS_F0_VAL		= <(player_start_y_F0-player_start_y)
COURSE_SCENERY_ENTER_POS_00_A	= (COURSE_SCENERY_ENTER_POS_00_A_VAL<<COURSE_SCENERY_ENTER_POS_SHIFT)
COURSE_SCENERY_ENTER_POS_20	= (COURSE_SCENERY_ENTER_POS_20_VAL<<COURSE_SCENERY_ENTER_POS_SHIFT)
COURSE_SCENERY_ENTER_POS_B0_A	= (COURSE_SCENERY_ENTER_POS_B0_A_VAL<<COURSE_SCENERY_ENTER_POS_SHIFT)
COURSE_SCENERY_ENTER_POS_50	= (COURSE_SCENERY_ENTER_POS_50_VAL<<COURSE_SCENERY_ENTER_POS_SHIFT)
COURSE_SCENERY_ENTER_POS_00_B	= (COURSE_SCENERY_ENTER_POS_00_B_VAL<<COURSE_SCENERY_ENTER_POS_SHIFT)
COURSE_SCENERY_ENTER_POS_00_C	= (COURSE_SCENERY_ENTER_POS_00_C_VAL<<COURSE_SCENERY_ENTER_POS_SHIFT)
COURSE_SCENERY_ENTER_POS_B0_B	= (COURSE_SCENERY_ENTER_POS_B0_B_VAL<<COURSE_SCENERY_ENTER_POS_SHIFT)
COURSE_SCENERY_ENTER_POS_B0_C	= (COURSE_SCENERY_ENTER_POS_B0_C_VAL<<COURSE_SCENERY_ENTER_POS_SHIFT)
COURSE_SCENERY_ENTER_POS_MAX	= COURSE_SCENERY_ENTER_POS_B0_C_VAL

COURSE_TIMER_SETTING_SHIFT = 6
COURSE_TIMER_SETTING_0	= (0<<COURSE_TIMER_SETTING_SHIFT)
COURSE_TIMER_SETTING_1	= (1<<COURSE_TIMER_SETTING_SHIFT)
COURSE_TIMER_SETTING_2	= (2<<COURSE_TIMER_SETTING_SHIFT)
COURSE_TIMER_SETTING_3	= (3<<COURSE_TIMER_SETTING_SHIFT)
COURSE_TIMER_SETTING_MAX = 3

COURSE_FLOOR_MASK_SHIFT	= 8
COURSE_FLOOR_MASK_0_VAL	= ((floor_mask_0-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_1_VAL	= ((floor_mask_1-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_2_VAL	= ((floor_mask_2-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_3_VAL	= ((floor_mask_3-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_4_VAL	= ((floor_mask_4-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_5_VAL	= ((floor_mask_5-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_6_VAL	= ((floor_mask_6-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_7_VAL	= ((floor_mask_7-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_8_VAL	= ((floor_mask_8-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_9_VAL	= ((floor_mask_9-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_10_VAL	= ((floor_mask_10-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_11_VAL	= ((floor_mask_11-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_12_VAL	= ((floor_mask_12-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_13_VAL	= ((floor_mask_13-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_14_VAL	= ((floor_mask_14-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_15_VAL	= ((floor_mask_15-scenery_floor_masks)/WORD_SIZE)
COURSE_FLOOR_MASK_0	= (COURSE_FLOOR_MASK_0_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_1	= (COURSE_FLOOR_MASK_1_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_2	= (COURSE_FLOOR_MASK_2_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_3	= (COURSE_FLOOR_MASK_3_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_4	= (COURSE_FLOOR_MASK_4_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_5	= (COURSE_FLOOR_MASK_5_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_6	= (COURSE_FLOOR_MASK_6_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_7	= (COURSE_FLOOR_MASK_7_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_8	= (COURSE_FLOOR_MASK_8_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_9	= (COURSE_FLOOR_MASK_9_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_10	= (COURSE_FLOOR_MASK_10_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_11	= (COURSE_FLOOR_MASK_11_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_12	= (COURSE_FLOOR_MASK_12_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_13	= (COURSE_FLOOR_MASK_13_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_14	= (COURSE_FLOOR_MASK_14_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_15	= (COURSE_FLOOR_MASK_15_VAL<<COURSE_FLOOR_MASK_SHIFT)
COURSE_FLOOR_MASK_MAX	= <(COURSE_FLOOR_MASK_15>>COURSE_FLOOR_MASK_SHIFT)

COURSE_BG_SHIFT		= 12
COURSE_BG_NONE_VAL	= 0
COURSE_BG_CLOUD_VAL	= (scenery_static_tbl_cloud-scenery_static_tbl+1)
COURSE_BG_MNT_BUSH_VAL	= (scenery_static_tbl_mnt_bush-scenery_static_tbl+1)
COURSE_BG_TREE_FENCE_VAL = (scenery_static_tbl_tree_fence-scenery_static_tbl+1)
COURSE_BG_NONE		= (COURSE_BG_NONE_VAL<<COURSE_BG_SHIFT)
COURSE_BG_CLOUD		= (COURSE_BG_CLOUD_VAL<<COURSE_BG_SHIFT)
COURSE_BG_MNT_BUSH	= (COURSE_BG_MNT_BUSH_VAL<<COURSE_BG_SHIFT)
COURSE_BG_TREE_FENCE	= (COURSE_BG_TREE_FENCE_VAL<<COURSE_BG_SHIFT)
COURSE_BG_MAX		= <(COURSE_BG_TREE_FENCE>>COURSE_BG_SHIFT)

COURSE_AREA_LEDGE_SHIFT		= 14
COURSE_AREA_LEDGE_TREE_VAL	= <((scenery_ledge_tbl_tree-scenery_ledge_tbl)/WORD_SIZE)
COURSE_AREA_LEDGE_TREE		= (COURSE_AREA_LEDGE_TREE_VAL<<COURSE_AREA_LEDGE_SHIFT)
COURSE_AREA_LEDGE_MUSHROOM_VAL	= <((scenery_ledge_tbl_mushroom-scenery_ledge_tbl)/WORD_SIZE)
COURSE_AREA_LEDGE_MUSHROOM	= (COURSE_AREA_LEDGE_MUSHROOM_VAL<<COURSE_AREA_LEDGE_SHIFT)
COURSE_AREA_LEDGE_CANNON_VAL	= <((scenery_ledge_tbl_cannon-scenery_ledge_tbl)/WORD_SIZE)
COURSE_AREA_LEDGE_CANNON	= (COURSE_AREA_LEDGE_CANNON_VAL<<COURSE_AREA_LEDGE_SHIFT)
COURSE_AREA_LEDGE_CLOUD_VAL	= 3
COURSE_AREA_LEDGE_CLOUD		= (COURSE_AREA_LEDGE_CLOUD_VAL<<COURSE_AREA_LEDGE_SHIFT)
COURSE_AREA_LEDGE_MAX		= <(COURSE_AREA_LEDGE_CLOUD>>COURSE_AREA_LEDGE_SHIFT)

COURSE_SCENERY_POS_Y_0	= (0<<0)
COURSE_SCENERY_POS_Y_1	= (1<<0)
COURSE_SCENERY_POS_Y_2	= (2<<0)
COURSE_SCENERY_POS_Y_3	= (3<<0)
COURSE_SCENERY_POS_Y_4	= (4<<0)
COURSE_SCENERY_POS_Y_5	= (5<<0)
COURSE_SCENERY_POS_Y_6	= (6<<0)
COURSE_SCENERY_POS_Y_7	= (7<<0)
COURSE_SCENERY_POS_Y_8	= (8<<0)
COURSE_SCENERY_POS_Y_9	= (9<<0)
COURSE_SCENERY_POS_Y_A	= ($A<<0)
COURSE_SCENERY_POS_Y_B	= ($B<<0)
COURSE_SCENERY_TYPE_C	= ($C<<0)
COURSE_SCENERY_TYPE_D	= ($D<<0)
COURSE_SCENERY_TYPE_E	= ($E<<0)
COURSE_SCENERY_TYPE_F	= ($F<<0)
COURSE_SCENERY_TYPE_MASK = ($F<<0)

COURSE_SCENERY_POS_X_0	= (0<<4)
COURSE_SCENERY_POS_X_1	= (1<<4)
COURSE_SCENERY_POS_X_2	= (2<<4)
COURSE_SCENERY_POS_X_3	= (3<<4)
COURSE_SCENERY_POS_X_4	= (4<<4)
COURSE_SCENERY_POS_X_5	= (5<<4)
COURSE_SCENERY_POS_X_6	= (6<<4)
COURSE_SCENERY_POS_X_7	= (7<<4)
COURSE_SCENERY_POS_X_8	= (8<<4)
COURSE_SCENERY_POS_X_9	= (9<<4)
COURSE_SCENERY_POS_X_A	= ($A<<4)
COURSE_SCENERY_POS_X_B	= ($B<<4)
COURSE_SCENERY_POS_X_C	= ($C<<4)
COURSE_SCENERY_POS_X_D	= ($D<<4)
COURSE_SCENERY_POS_X_E	= ($E<<4)
COURSE_SCENERY_POS_X_F	= ($F<<4)
COURSE_SCENERY_POSX_MASK = ($F<<4)

OBJ_BASE_BIT	= 12
COURSE_OBJ_PIPE_VERT_A	= (((scenery_tbl_pipe_vert_a-scenery_norm)/WORD_SIZE)<<OBJ_BASE_BIT)
COURSE_OBJ_LEDGE	= (((scenery_tbl_ledge-scenery_norm)/WORD_SIZE)<<OBJ_BASE_BIT)
COURSE_OBJ_BRICKROW	= (((scenery_tbl_brickrow-scenery_norm)/WORD_SIZE)<<OBJ_BASE_BIT)
COURSE_OBJ_FLOORROW	= (((scenery_tbl_floorrow-scenery_norm)/WORD_SIZE)<<OBJ_BASE_BIT)
COURSE_OBJ_COINROW	= (((scenery_tbl_coinrow-scenery_norm)/WORD_SIZE)<<OBJ_BASE_BIT)
COURSE_OBJ_BRICKCOL	= (((scenery_tbl_brickcol-scenery_norm)/WORD_SIZE)<<OBJ_BASE_BIT)
COURSE_OBJ_FLOORCOL	= (((scenery_tbl_floorcol-scenery_norm)/WORD_SIZE)<<OBJ_BASE_BIT)
COURSE_OBJ_PIPE_VERT_B	= (((scenery_tbl_pipe_vert_b-scenery_norm)/WORD_SIZE)<<OBJ_BASE_BIT)

OBJ_A_BIT	= 12
COURSE_OBJ_A_PIT	= (((scenery_tbl_pit-scenery_spec_A)/WORD_SIZE)<<OBJ_A_BIT)
COURSE_OBJ_A_PULLEY	= (((scenery_tbl_pulley-scenery_spec_A)/WORD_SIZE)<<OBJ_A_BIT)
COURSE_OBJ_A_BRIDGE_HI	= (((scenery_tbl_bridge_hi-scenery_spec_A)/WORD_SIZE)<<OBJ_A_BIT)
COURSE_OBJ_A_BRIDGE_MID	= (((scenery_tbl_bridge_mid-scenery_spec_A)/WORD_SIZE)<<OBJ_A_BIT)
COURSE_OBJ_A_BRIDGE_LO	= (((scenery_tbl_bridge_lo-scenery_spec_A)/WORD_SIZE)<<OBJ_A_BIT)
COURSE_OBJ_A_WATERHOLE	= (((scenery_tbl_waterhole-scenery_spec_A)/WORD_SIZE)<<OBJ_A_BIT)
COURSE_OBJ_A_BLOCK_QHI	= (((scenery_tbl_block_qhi-scenery_spec_A)/WORD_SIZE)<<OBJ_A_BIT)
COURSE_OBJ_A_BLOCK_QLO	= (((scenery_tbl_block_qlo-scenery_spec_A)/WORD_SIZE)<<OBJ_A_BIT)

COURSE_OBJ_DIM_1	= (($0)<<8)
COURSE_OBJ_DIM_2	= (($1)<<8)
COURSE_OBJ_DIM_3	= (($2)<<8)
COURSE_OBJ_DIM_4	= (($3)<<8)
COURSE_OBJ_DIM_5	= (($4)<<8)
COURSE_OBJ_DIM_6	= (($5)<<8)
COURSE_OBJ_DIM_7	= (($6)<<8)
COURSE_OBJ_DIM_8	= (($7)<<8)
COURSE_OBJ_DIM_9	= (($8)<<8)
COURSE_OBJ_DIM_10	= (($9)<<8)
COURSE_OBJ_DIM_11	= (($A)<<8)
COURSE_OBJ_DIM_12	= (($B)<<8)
COURSE_OBJ_DIM_13	= (($C)<<8)
COURSE_OBJ_DIM_14	= (($D)<<8)
COURSE_OBJ_DIM_15	= (($E)<<8)
COURSE_OBJ_DIM_16	= (($F)<<8)

COURSE_SCENERY_TYPE_D_LOC_MASK	= $1F00
COURSE_SCENERY_TYPE_D_OBJ_MASK	= $3F00
COURSE_SCENERY_TYPE_D_MASK	= $4000

COURSE_PAGE_D_BIT	= 8
COURSE_PAGE_D_1		= (1<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_2		= (2<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_3		= (3<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_4		= (4<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_5		= (5<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_6		= (6<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_7		= (7<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_8		= (8<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_9		= (9<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_A		= ($A<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_B		= ($B<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_C		= ($C<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_D		= ($D<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_E		= ($E<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_F		= ($F<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_10	= ($10<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_11	= ($11<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_12	= ($12<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_13	= ($13<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_14	= ($14<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_15	= ($15<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_16	= ($16<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_17	= ($17<<COURSE_PAGE_D_BIT)
COURSE_PAGE_D_18	= ($18<<COURSE_PAGE_D_BIT)


COURSE_DOWNPIPE_MASK	= ($7800)

OBJ_D_BIT	= 8
COURSE_OBJ_D_PIPE_INTRO	= (((scenery_tbl_pipe_intro-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
COURSE_OBJ_D_POLE	= (((scenery_tbl_pole-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
COURSE_OBJ_D_BRIDGE_AXE	= (((scenery_tbl_castle_bridge_axe-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
COURSE_OBJ_D_BRIDGE_DRAW = (((scenery_tbl_castle_bridge_draw-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
COURSE_OBJ_D_BRIDGE	= (((scenery_tbl_castle_bridge-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
COURSE_OBJ_D_LOCK_WARP	= (((scenery_tbl_lock_warp-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
COURSE_OBJ_D_LOCK_END	= (((scenery_tbl_lock_end-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
COURSE_OBJ_D_LOCK_START	= (((scenery_tbl_lock_start-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
COURSE_OBJ_D_SWARM_A	= (((scenery_tbl_swarm_a-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
COURSE_OBJ_D_SWARM_B	= (((scenery_tbl_swarm_b-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
COURSE_OBJ_D_SWARM_C	= (((scenery_tbl_swarm_c-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
COURSE_OBJ_D_LOOP	= (((scenery_tbl_decode_rts-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
.if	.defined(SMB2)
	COURSE_OBJ_D_WIND_ON	= (((scenery_tbl_wind_on-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
	COURSE_OBJ_D_WIND_OFF	= (((scenery_tbl_wind_off-scenery_spec_D)/WORD_SIZE)<<OBJ_D_BIT)
.endif

COURSE_SCENERY_OBJ_MASK	= $7000

OBJ_B_BIT	= 12
COURSE_OBJ_B_ROPE_NORM	= (((scenery_tbl_rope_norm-scenery_spec_B)/WORD_SIZE)<<OBJ_B_BIT)
COURSE_OBJ_B_ROPE_PLAT	= (((scenery_tbl_rope_plat-scenery_spec_B)/WORD_SIZE)<<OBJ_B_BIT)
COURSE_OBJ_B_CASTLE	= (((scenery_tbl_castle-scenery_spec_B)/WORD_SIZE)<<OBJ_B_BIT)
COURSE_OBJ_B_STAIRS	= (((scenery_tbl_stairs-scenery_spec_B)/WORD_SIZE)<<OBJ_B_BIT)
COURSE_OBJ_B_PIPE_EXIT	= (((scenery_tbl_pipe_exit-scenery_spec_B)/WORD_SIZE)<<OBJ_B_BIT)
COURSE_OBJ_B_POLE_TIP	= (((scenery_tbl_pole_tip-scenery_spec_B)/WORD_SIZE)<<OBJ_B_BIT)
.if	.defined(SMBV2)
COURSE_OBJ_B_PIPE_V_FLIP_A = (((scenery_tbl_pipe_v_flip_a-scenery_spec_B)/WORD_SIZE)<<OBJ_B_BIT)
COURSE_OBJ_B_PIPE_V_FLIP_B = (((scenery_tbl_pipe_v_flip_b-scenery_spec_B)/WORD_SIZE)<<OBJ_B_BIT)
.endif

OBJ_C_BIT	= 8
COURSE_OBJ_C_BLOCK_Q_A		= (((scenery_tbl_block_q_a-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
COURSE_OBJ_C_BLOCK_Q_B		= (((scenery_tbl_block_q_b-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
COURSE_OBJ_C_BLOCK_Q_C		= (((scenery_tbl_block_q_c-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
.if .defined(SMB2)
	COURSE_OBJ_C_BLOCK_Q_D		= (((scenery_tbl_block_q_d-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
.endif
COURSE_OBJ_C_HIDDEN_ONE_UP	= (((scenery_tbl_hidden_one_up-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
.if .defined(SMBV2)
	COURSE_OBJ_C_BLOCK_Q_E		= (((scenery_tbl_block_q_e-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
.endif
.if	.defined(SMB2)
	COURSE_OBJ_C_BLOCK_Q_F		= (((scenery_tbl_block_q_f-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
	COURSE_OBJ_C_BLOCK_ITEM_E	= (((scenery_tbl_block_item_e-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
.endif
COURSE_OBJ_C_BLOCK_ITEM_A	= (((scenery_tbl_block_item_a-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
COURSE_OBJ_C_BLOCK_ITEM_B	= (((scenery_tbl_block_item_b-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
COURSE_OBJ_C_BLOCK_ITEM_C	= (((scenery_tbl_block_item_c-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
COURSE_OBJ_C_BLOCK_COIN		= (((scenery_tbl_block_coin-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
COURSE_OBJ_C_BLOCK_ITEM_D	= (((scenery_tbl_block_item_d-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
COURSE_OBJ_C_PIPE_WATER		= (((scenery_tbl_pipe_water-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
COURSE_OBJ_C_EMPTY_DRAW		= (((scenery_tbl_empty_draw-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
COURSE_OBJ_C_SPRING		= (((scenery_tbl_spring-scenery_spec_C)/WORD_SIZE)<<OBJ_C_BIT)
COURSE_OBJ_C_MASK		= $0F00

COURSE_OBJ_E_FLOOR_MASK	= $4000
COURSE_OBJ_E_MASK_UNK	= $2000
COURSE_OBJ_E_MASK_UNK_B	= $1000

COURSE_SCENERY_NEW_PAGE	= (1<<15)

COURSE_SCENERY_END	= <(COURSE_SCENERY_TYPE_D|COURSE_SCENERY_POS_X_F)

COURSE_ACTORS_POS_Y_0	= (0<<0)
COURSE_ACTORS_POS_Y_1	= (1<<0)
COURSE_ACTORS_POS_Y_2	= (2<<0)
COURSE_ACTORS_POS_Y_3	= (3<<0)
COURSE_ACTORS_POS_Y_4	= (4<<0)
COURSE_ACTORS_POS_Y_5	= (5<<0)
COURSE_ACTORS_POS_Y_6	= (6<<0)
COURSE_ACTORS_POS_Y_7	= (7<<0)
COURSE_ACTORS_POS_Y_8	= (8<<0)
COURSE_ACTORS_POS_Y_9	= (9<<0)
COURSE_ACTORS_POS_Y_A	= ($A<<0)
COURSE_ACTORS_POS_Y_B	= ($B<<0)
COURSE_ACTORS_POS_Y_C	= ($C<<0)
COURSE_ACTORS_POS_Y_D	= ($D<<0)
COURSE_ACTORS_TYPE_E	= ($E<<0)
COURSE_ACTORS_TYPE_F	= ($F<<0)
COURSE_ACTORS_TYPE_MASK	= ($F<<0)

COURSE_ACTORS_POS_X_0	= (0<<4)
COURSE_ACTORS_POS_X_1	= (1<<4)
COURSE_ACTORS_POS_X_2	= (2<<4)
COURSE_ACTORS_POS_X_3	= (3<<4)
COURSE_ACTORS_POS_X_4	= (4<<4)
COURSE_ACTORS_POS_X_5	= (5<<4)
COURSE_ACTORS_POS_X_6	= (6<<4)
COURSE_ACTORS_POS_X_7	= (7<<4)
COURSE_ACTORS_POS_X_8	= (8<<4)
COURSE_ACTORS_POS_X_9	= (9<<4)
COURSE_ACTORS_POS_X_A	= ($A<<4)
COURSE_ACTORS_POS_X_B	= ($B<<4)
COURSE_ACTORS_POS_X_C	= ($C<<4)
COURSE_ACTORS_POS_X_D	= ($D<<4)
COURSE_ACTORS_POS_X_E	= ($E<<4)
COURSE_ACTORS_POS_X_F	= ($F<<4)
COURSE_ACTORS_POSX_MASK	= ($F<<4)

COURSE_ACTORS_PAGE_LOC_BIT	= 8
COURSE_ACTORS_PAGE_LOC_01	= ($01<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_02	= ($02<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_03	= ($03<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_04	= ($04<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_05	= ($05<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_06	= ($06<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_07	= ($07<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_08	= ($08<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_09	= ($09<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_0A	= ($0A<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_0B	= ($0B<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_0C	= ($0C<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_0D	= ($0D<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_0E	= ($0E<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_0F	= ($0F<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_10	= ($10<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_11	= ($11<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_12	= ($12<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_13	= ($13<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_14	= ($14<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_15	= ($15<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_16	= ($16<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_17	= ($17<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_18	= ($18<<COURSE_ACTORS_PAGE_LOC_BIT)
COURSE_ACTORS_PAGE_LOC_MASK	= (%111111<<COURSE_ACTORS_PAGE_LOC_BIT)

COURSE_ACTORS_ID_BIT		= 8
COURSE_ACTORS_ID_KOOPA_GREEN	= (actor::koopa_green<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_BUZZY_BEETLE	= (actor::buzzy_beetle<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_KOOPA_RED	= (actor::koopa_red<<COURSE_ACTORS_ID_BIT)
.if	.defined(SMBV2)
COURSE_ACTORS_ID_PIRANHA_PLANT_B	= (actor::piranha_plant_b<<COURSE_ACTORS_ID_BIT)
.endif
COURSE_ACTORS_ID_HAMMER_BRO	= (actor::hammer_bro<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_GOOMBA		= (actor::goomba<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_BLOOPER	= (actor::blooper<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_BULLET_SWARM	= (actor::bullet_swarm<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_CHEEP_GREY	= (actor::cheep_grey<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_CHEEP_RED	= (actor::cheep_red<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PODOBOO	= (actor::podoboo<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PIRANHA_PLANT	= (actor::piranha_plant<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PARA_GREEN_J	= (actor::koopa_para_jump_green<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PARA_RED_F	= (actor::koopa_para_fly_red<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PARA_GREEN_F	= (actor::koopa_para_fly_green<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_LAKITU		= (actor::lakitu<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_SPINY		= (actor::spiny<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_CHEEP_FLY	= (actor::cheep_fly<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_BOWSER_FLAME	= (actor::bowser_flame<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_FIREWORKS	= (actor::fireworks<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_BULLET_CHEEP_SWARM = (actor::bullet_cheep_swarm<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_SWARM_STOP	= (actor::swarm_stop<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_FIREBAR_CLOCK_SLOW = (actor::firebar_clock_slow<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_FIREBAR_CLOCK_FAST = (actor::firebar_clock_fast<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_FIREBAR_COUNTER_SLOW = (actor::firebar_counter_slow<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_FIREBAR_COUNTER_FAST = (actor::firebar_counter_fast<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_FIREBAR_LONG	= (actor::firebar_long<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PLAT_BAL	= (actor::plat_bal<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PLAT_STD_V	= (actor::plat_std_v<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PLAT_L_UP	= (actor::plat_l_up<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PLAT_L_DOWN	= (actor::plat_l_down<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PLAT_STD_H	= (actor::plat_std_h<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PLAT_DROP	= (actor::plat_drop<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PLAT_STD_H_B	= (actor::plat_std_h_b<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PLAT_S_UP	= (actor::plat_s_up<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_PLAT_S_DOWN	= (actor::plat_s_down<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_BOWSER		= (actor::bowser<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_POWER_UP	= (actor::power_up<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_VINE		= (actor::vine<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_FLAG_ENEMY	= (actor::flag_enemy<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_FLAG_VICTORY	= (actor::flag_victory<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_SPRING		= (actor::spring<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_BULLET		= (actor::bullet<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_WARP		= (actor::warp<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_TOAD		= (actor::toad<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_GOOMBA_DUO_LO	= (actor::goomba_duo_lo<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_GOOMBA_TRIO_LO	= (actor::goomba_trio_lo<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_GOOMBA_DUO_HI	= (actor::goomba_duo_hi<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_GOOMBA_TRIO_HI	= (actor::goomba_trio_hi<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_KOOPA_DUO_LO	= (actor::koopa_duo_lo<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_KOOPA_TRIO_LO	= (actor::koopa_trio_lo<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_KOOPA_DUO_HI	= (actor::koopa_duo_hi<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_KOOPA_TRIO_HI	= (actor::koopa_trio_hi<<COURSE_ACTORS_ID_BIT)
COURSE_ACTORS_ID_MASK		= (%111111<<COURSE_ACTORS_ID_BIT)

COURSE_ACTORS_SUBSPACE_ENTRY_BIT	= 8
.if	.defined(SMBM)|.defined(VS_SMB)
COURSE_ACTORS_SUBSPACE_WATER_01		= (course_water::no_01<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.if	.defined(SMBV1)
COURSE_ACTORS_SUBSPACE_WATER_03		= (course_water::no_03<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.elseif	.defined(ANN)
COURSE_ACTORS_SUBSPACE_WATER_04_SMB2		= (course_water::no_04_smb2<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.endif
COURSE_ACTORS_SUBSPACE_OVERWORLD_03	= (course_overworld::no_03<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_04	= (course_overworld::no_04<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_05	= (course_overworld::no_05<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_06	= (course_overworld::no_06<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_09	= (course_overworld::no_09<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_11	= (course_overworld::no_11<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_12	= (course_overworld::no_12<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_16	= (course_overworld::no_16<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_17	= (course_overworld::no_17<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_18	= (course_overworld::no_18<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_19	= (course_overworld::no_19<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_20	= (course_overworld::no_20<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_21	= (course_overworld::no_21<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.if	.defined(ANN)
	COURSE_ACTORS_SUBSPACE_OVERWORLD_28_SMB2	= (course_overworld::no_28_smb2<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
	COURSE_ACTORS_SUBSPACE_OVERWORLD_26_ANN		= (course_overworld::no_26_ann<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
	COURSE_ACTORS_SUBSPACE_OVERWORLD_27_ANN		= (course_overworld::no_27_ann<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.endif
COURSE_ACTORS_SUBSPACE_UNDERGROUND_01	= (course_underground::no_01<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_UNDERGROUND_02	= (course_underground::no_02<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_UNDERGROUND_03	= (course_underground::no_03<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.if	.defined(ANN)
COURSE_ACTORS_SUBSPACE_UNDERGROUND_04_ANN	= (course_underground::no_04_ann<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.endif
.if	.defined(SMBV1)
COURSE_ACTORS_SUBSPACE_CASTLE_06	= (course_castle::no_06<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.elseif	.defined(ANN)
COURSE_ACTORS_SUBSPACE_CASTLE_08_SMB2	= (course_castle::no_08_smb2<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.endif
.elseif	.defined(SMB2)
COURSE_ACTORS_SUBSPACE_WATER_01		= (course_water_smb2::no_01<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_WATER_03		= (course_water_smb2::no_03<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_WATER_04_SMB2	= (course_water_smb2::no_04<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_WATER_05		= (course_water_smb2::no_05<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_WATER_06		= (course_water_smb2::no_06<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_01	= (course_overworld_smb2::no_01<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_03	= (course_overworld_smb2::no_03<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_22_VS	= (course_overworld_smb2::no_22_vs<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_05	= (course_overworld_smb2::no_05<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_06	= (course_overworld_smb2::no_06<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_08	= (course_overworld_smb2::no_08<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_09	= (course_overworld_smb2::no_09<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_12	= (course_overworld_smb2::no_12<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_13	= (course_overworld_smb2::no_13<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_14	= (course_overworld_smb2::no_14<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_16	= (course_overworld_smb2::no_16<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_17	= (course_overworld_smb2::no_17<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_18	= (course_overworld_smb2::no_18<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_19	= (course_overworld_smb2::no_19<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_20	= (course_overworld_smb2::no_20<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_21	= (course_overworld_smb2::no_21<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_22	= (course_overworld_smb2::no_22<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_23	= (course_overworld_smb2::no_23<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_28_SMB2 = (course_overworld_smb2::no_28<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD_29	= (course_overworld_smb2::no_29<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_UNDERGROUND_01	= (course_underground_smb2::no_01<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_UNDERGROUND_02	= (course_underground_smb2::no_02<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_UNDERGROUND_03	= (course_underground_smb2::no_03<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_UNDERGROUND_05	= (course_underground_smb2::no_05<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_CASTLE_06	= (course_castle_smb2::no_06<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_CASTLE_08_SMB2	= (course_castle_smb2::no_08<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_CASTLE_09	= (course_castle_smb2::no_09<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_ACTORS_SUBSPACE_CASTLE_10	= (course_castle_smb2::no_10<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.endif

.if	.defined(SMBV2)
COURSE_EXT_ACTORS_SUBSPACE_CASTLE_02	= (course_castle_ext::no_02<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_EXT_ACTORS_SUBSPACE_CASTLE_04	= (course_castle_ext::no_04<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_01	= (course_overworld_ext::no_01<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_02	= (course_overworld_ext::no_02<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.if	.defined(SMB2)
	COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_03	= (course_overworld_ext::no_03<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.elseif	.defined(ANN)
	COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_12_SMB2	= (course_overworld_ext::no_12_smb2<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.endif
COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_05	= (course_overworld_ext::no_05<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.if	.defined(SMB2)
	COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_08	= (course_overworld_ext::no_08<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.elseif	.defined(ANN)
	COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_19_SMB2	= (course_overworld_ext::no_19_smb2<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.endif
COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_09	= (course_overworld_ext::no_09<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_11	= (course_overworld_ext::no_11<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_12	= (course_overworld_ext::no_12<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_EXT_ACTORS_SUBSPACE_OVERWORLD_14	= (course_overworld_ext::no_14<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
COURSE_EXT_ACTORS_SUBSPACE_UNDERGROUND_02 = (course_underground_ext::no_02<<COURSE_ACTORS_SUBSPACE_ENTRY_BIT)
.endif

COURSE_ACTORS_SUBSPACE_BIT		= 13
COURSE_ACTORS_SUBSPACE_WATER		= (course_types::water<<COURSE_ACTORS_SUBSPACE_BIT)
COURSE_ACTORS_SUBSPACE_OVERWORLD	= (course_types::overworld<<COURSE_ACTORS_SUBSPACE_BIT)
COURSE_ACTORS_SUBSPACE_UNDERGROUND	= (course_types::underground<<COURSE_ACTORS_SUBSPACE_BIT)
COURSE_ACTORS_SUBSPACE_CASTLE		= (course_types::castle<<COURSE_ACTORS_SUBSPACE_BIT)

COURSE_ACTORS_HARD_ONLY	= (1<<14)

COURSE_ACTORS_NEW_PAGE	= (1<<15)

COURSE_ACTORS_END	= (COURSE_ACTORS_TYPE_F|COURSE_ACTORS_POS_X_F)

COURSE_ACTORS_TYPE_E_STARTPAGE_BIT	= 0
COURSE_ACTORS_TYPE_E_STARTPAGE_00	= ($00<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_01	= ($01<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_02	= ($02<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_03	= ($03<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_04	= ($04<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_05	= ($05<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_06	= ($06<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_07	= ($07<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_08	= ($08<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_09	= ($09<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_0A	= ($0A<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_0B	= ($0B<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_0C	= ($0C<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_0D	= ($0D<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_0E	= ($0E<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_0F	= ($0F<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_10	= ($10<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_11	= ($11<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_12	= ($12<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_13	= ($13<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_14	= ($14<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_15	= ($15<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_16	= ($16<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_17	= ($17<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_18	= ($18<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)
COURSE_ACTORS_TYPE_E_STARTPAGE_MASK	= (%11111<<COURSE_ACTORS_TYPE_E_STARTPAGE_BIT)

COURSE_ACTORS_TYPE_E_COURSE_NO_BIT	= 5
COURSE_ACTORS_TYPE_E_COURSE_NO_1	= (course::no_01<<COURSE_ACTORS_TYPE_E_COURSE_NO_BIT)
COURSE_ACTORS_TYPE_E_COURSE_NO_2        = (course::no_02<<COURSE_ACTORS_TYPE_E_COURSE_NO_BIT)
COURSE_ACTORS_TYPE_E_COURSE_NO_3        = (course::no_03<<COURSE_ACTORS_TYPE_E_COURSE_NO_BIT)
COURSE_ACTORS_TYPE_E_COURSE_NO_4        = (course::no_04<<COURSE_ACTORS_TYPE_E_COURSE_NO_BIT)
COURSE_ACTORS_TYPE_E_COURSE_NO_5        = (course::no_05<<COURSE_ACTORS_TYPE_E_COURSE_NO_BIT)
COURSE_ACTORS_TYPE_E_COURSE_NO_6        = (course::no_06<<COURSE_ACTORS_TYPE_E_COURSE_NO_BIT)
COURSE_ACTORS_TYPE_E_COURSE_NO_7        = (course::no_07<<COURSE_ACTORS_TYPE_E_COURSE_NO_BIT)
COURSE_ACTORS_TYPE_E_COURSE_NO_8        = (course::no_08<<COURSE_ACTORS_TYPE_E_COURSE_NO_BIT)
COURSE_ACTORS_TYPE_E_COURSE_NO_MASK	= (%111<<COURSE_ACTORS_TYPE_E_COURSE_NO_BIT)

COURSE_DESC_PAGE_BIT	= 0

COURSE_DESC_TYPE_BIT	= 5
COURSE_TYPE_WATER_B		= (course_types::water<<COURSE_DESC_TYPE_BIT)
COURSE_TYPE_OVERWORLD_B		= (course_types::overworld<<COURSE_DESC_TYPE_BIT)
COURSE_TYPE_UNDERGROUND_B	= (course_types::underground<<COURSE_DESC_TYPE_BIT)
COURSE_TYPE_CASTLE_B		= (course_types::castle<<COURSE_DESC_TYPE_BIT)
COURSE_TYPE_CLOUD_B		= (course_types::cloud<<COURSE_DESC_TYPE_BIT)

.endif	; COURSE_FLAGS_I

