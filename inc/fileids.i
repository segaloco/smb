.ifndef FILEIDS_I
FILEIDS_I = 1

; TODO - Generate data/fs/files/*.defs from these

FILE_ID_CHAR1		= $01
FILE_ID_CHAR2		= $10

FILE_ID_MAIN		= $05

FILE_ID_SAVE		= $0F
FILE_NO_SAVE		= 7

FILE_ID_DATA2		= $20
FILE_ID_DATA3		= $30
FILE_ID_DATA4		= $40

FILE_ID_NULL		= $FF

.endif	; FILEIDS_I
